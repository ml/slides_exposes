# Introduction

## Note

Ce site compile les supports d'exposés donnés en 2021 dans le cadre du groupe de travail Apprentissage Machine de l'IRMA.

Il est disponible sous forme de livre au format pdf : {download}`ml-irma.pdf <ml-irma.pdf>`.

:::{note}
Le contenu évolue au fur et à mesure des exposés.
:::

```{include} ../README.md
```