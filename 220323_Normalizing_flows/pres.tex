\documentclass[9pt]{beamer}

\input{preambule}

\title[GT ML]{Normalizing Flows}
\author[R. Hild]{Romain Hild}

\date{March 23, 2022}


%\code{E-mail:}
\institute[IRMA]{Universté de Strasbourg - IRMA}

\begin{document}

\begin{frame}[plain]
  \maketitle
\end{frame}

\begin{frame}{Different types of Generative Models}
  \begin{itemize}
    \item 
    Generative Adversarial Networks: Model the data generation as a supervised problem, by using a discriminator model and a generator model.
    The models are trained by optimizing a minmax problem.
    \vfill
    \item 
    Variational AutoEncoders: Two neural networks inexplicitly optimizes the log-likelihood of the data by maximizing the evidence lower bound (ELBO).
    \vfill
    \item Flow-based Generative Networks: Using a sequence of invertible transformations, the models try to learn the data distribution $p(\bx)$ of the observed data.
  \end{itemize}
\end{frame}

\begin{frame}{Different types of Generative Models}
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/GNs.png}
    \caption{Comparison of three categories of generative models \cite{weng2018flow}}
  \end{figure}
\end{frame}

\begin{frame}{Density estimation}
  Flow based generative models can learn the probability density function of real data, $p(\bx)$, by using normalizing flows.\\
  \vfill
  This allows to do:
  \begin{itemize}
    \item data generation: drawing statistically independent one-shot samples of $p(\bx)$ 
    \item density estimation: predict the rareness of future events
    \item sample completion: fill in incomplete data samples
  \end{itemize}
\end{frame}

\begin{frame}{Normalizing flows}
  \begin{block}{Definition}
    \vfill
    Let $\bx\in \mathbb{R}^d$ be a continuous, real random variable, and $p_x(\bx)$ its probability distribution.
    Since it is too complicated to use directly $p_x(\bx)$, we want to approach it using flow-based modeling.\\~\\
    \vfill
    Express $\bx$ as a transformation $f$ of $\bz$, sampled from a simpler distribution $p_z(\bz)$:
    \begin{equation*}
      \bx = f(\bz) \quad \text{with} \quad \bz \sim p_z(\bz)
    \end{equation*}
    \vfill
    The training parameters $\theta = \{\phi, \psi\}$ define the transformation $f_\phi$ and the base distribution $p_z(\bz;\psi)$.\\~\\
    For flow models to be tractable, $f$ must be a diffeomorphism, so that we can use its Jacobian to write:
    \begin{equation*}
      p_x(\bx)=p_z(\bz)|\text{det}J_f(\bz)^{-1}|
    \end{equation*}
  \end{block}
\end{frame}

\begin{frame}{Normalizing flows}
  Using only one diffeomorphism can limit the expressivity of the family $p_x(\bx;\theta)$.
  We will chain the transformations to create a "flow", and normalize it by its Jacobian's determinant.
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/flows.png}
    \caption{\cite{janosh2019flow}}
  \end{figure}
  We have:
  \begin{gather*}
    \bx = f_k\circ \dots\circ f_1(\bz_0) \qquad \bz_0 \sim p_0(\bz_0)\\
    \bx\sim p_x(\bx) = p_0(\bz_0)\prod_{i=1}^k\left|\text{det}\frac{\partial f_k}{\partial \bz_{k-1}}\right|^{-1}
  \end{gather*}
\end{frame}

\begin{frame}{Loss}
  To compute the loss we need the log of the distribution:
  \begin{equation*}
    \log{p_x(\bx)} = \log{p_0(\bz_0)} - \sum_{i=1}^k \log{\left|\text{det}\frac{\partial f_i}{\partial \bz_{i-1}}\right|}
  \end{equation*}
  To fit the model, we use the Kullback–Leibler (KL) divergence to find the best parameters $\theta=\{\phi,\psi\}$ minimizing the divergence between the output $p_x(\bx;\theta)$ and the target distribution $p_x^*(\bx)$.\\
  \vfill
  We can write it by:
  \begin{itemize}
    \item Sampling the target density\\
      Useful to generate samples $\bx=f(\bz)$ with $\bz\sim p_z(\bz;\psi)$\\
      Need to compute $p_z(\bz;\psi)$, $f_\phi$, and diffentiate $f_\phi^{-1}$, $\text{det}J_{f_\phi^{-1}}$, $p_z(\bz;\psi)$
    \item Sampling the base density\\
      Useful to evaluate the model's density $p_x(\bx;\theta)=p_z(\bz;\psi)\left|\text{det}J_{f_\phi}(\bz)\right|^{-1}$\\
      Need to compute $f_\phi^{-1}$, $\text{det}J_{f_\phi}$, $p_z(\bz;\psi)$ and diffentiate $f_\phi$ and $\text{det}J_{f_\phi}$
  \end{itemize}
\end{frame}

\begin{frame}{Sampling the target density}
  \vspace{-1em}
  \begin{align*}
    \mathcal{L}(\theta) &= D_{KL}(p_x^* || p_x(\bx,\theta))\\
    &= \int_{\mathbb{R}^d} p_x^*(\bx) \log \left(\frac{p_x^*(\bx)}{p_x(\bx;\theta)}\right)\text{d}\bx\\
    &= -\int_{\mathbb{R}^d} p_x^*(x) \log p_x(\bx;\theta) \text{d}\bx + \int_{\mathbb{R}^d} p_x^* \log p_x^*(\bx)\text{d}\bx\\
    &= -\mathbb{E}_{p_x^*}\left(\log p_x(\bx;\theta)\right) + \text{cst}\\
    &= -\mathbb{E}_{p_x^*}\left[\log p_z\left(f_\phi^{-1}(\bx);\psi\right) + \log\left|\text{det} J_{f_\phi^{-1}}(\bx)\right|\right] + \text{cst}
  \end{align*}
  Which leads for a set of sample $\{\bx_n\}_{n=1}^N$ from $p_x^*(\bx)$ to:
  %Given a set of sample $\{\bx_n\}_{n=1}^N$ from $p_x^*(\bx)$, we can obtain an unbiased Monte Carlo estimate of $\mathcal{L}(\theta)$:
  \begin{equation*}
    \mathcal{L}(\theta)\approx -\frac{1}{N}\sum_{n=1}^N \left[\log p_z(f_\phi^{-1}(\bx_n);\psi)+\log\left|\text{det}J_{f_\phi^{-1}}(\bx_n)\right|\right] + \text{cst}
  \end{equation*}
  and its gradient with respect to $\theta$:
  \begin{align*}
    \nabla_\phi\mathcal{L}(\theta) &\approx -\frac{1}{N}\sum_{n=1}^N \nabla_\phi\log p_z(f_\phi^{-1}(\bx_n);\psi)+\nabla_\phi\log\left|\text{det}J_{f_\phi^{-1}}(\bx_n)\right|\\
    \nabla_\psi\mathcal{L}(\theta) &\approx -\frac{1}{N}\sum_{n=1}^N \nabla_\psi\log p_z(f_\phi^{-1}(\bx_n);\psi)
  \end{align*}
\end{frame}

\begin{frame}{Sampling the base density}
  \begin{align*}
    \mathcal{L}(\theta) &= D_{KL}(p_x(\bx,\theta) || p_x^*)\\
    &=\mathbb{E}_{p_x(\bx;\theta)}\left(\log p_x(\bx;\theta) - \log p_x^*(\bx)\right)\\
    &=\mathbb{E}_{p_z(\bz;\psi)}\left(\log p_z(\bz;\psi) - \log\left|\text{det}J_{f_\phi}(\bz)\right| - \log p_x^*(f_\phi(\bz))\right)
  \end{align*}
  Which leads for a set of sample $\{\bz_n\}_{n=1}^N$ from $p_z(\bz;\psi)$ to:
  \begin{equation*}
    \nabla_\phi\mathcal{L}(\theta) \approx -\frac{1}{N}\sum_{n=1}^N \nabla_\phi\log\left|\text{det} J_{f_\phi}(\bz_n)\right| + \nabla_\phi \log p_x^*(f_\phi(\bz_n))
  \end{equation*}

  \textit{"This loss function is chosen when using normalizing flows in the context of e.g. variational inference or model distillation. 
  The latter is an exciting application in which a flow model is trained to replace a target model whose density $p_x^*(\bx)$ can be evaluated but is otherwise inconvenient, e.g. difficult to sample."}\cite{janosh2019flow}
  Two examples of such application can be found in \cite{Oord2017,Noe2019}.
\end{frame}

\begin{frame}{Planar and Radial Flow}
  \begin{itemize}
    \item Planar Flow
    \begin{equation*}
      f(\bz) = \bz+\bu h(\bw^T\bz+b)
    \end{equation*}
    with $\bu$, $\bw\in\mathbb{R}^d$ and $b\in\mathbb{R}$ and $h$ a non linearity.
    If $\psi(\bz)=h'(\bw^T\bz+b)\bw$ we have:
    \begin{equation*}
      \left|\text{det}\frac{\partial f}{\partial \bz}\right| = |1+\bu^T\psi(\bz)|
    \end{equation*}
    \item Radial Flow
    \begin{equation*}
      f(\bz)=\bz+\beta h(\alpha,r)(\bz-\bz_0)
    \end{equation*}
    with $r=\|\bz-\bz_0\|_2$, $h(\alpha,r)=\frac{1}{\alpha+r}$ and parameters $\bz_0\in\mathbb{R}^d$,$\alpha\in\mathbb{R}_+$ and $\beta\in\mathbb{R}$.
  \end{itemize}
  \vspace{-0.5em}
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/planar-radial.png}
    \caption{\cite{kosiorek2018flow}}
  \end{figure}
\end{frame}

\begin{frame}{Autoregressive Flow}
  We can build transformations whose Jacobian are easy to compute by using only the previous dimensions $1:i$ to compute the $i$-th dimension:
  \begin{equation*}
    y_i=f(\bz_{1:i}), \qquad \mathbf{J} = \frac{\partial \mathbf{y}}{\partial \bz}
  \end{equation*}
  Then the Jacobian is triangular inferior and its determinant is the product of the terms of the diagonal:
  \begin{equation*}
    \text{det}\mathbf{J} = \prod_{j=1}^d \mathbf{J}_{ii}
  \end{equation*}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
  \nocite{*}
  \bibliographystyle{amsalpha}
  \bibliography{biblio.bib}
\end{frame}

\end{document}
