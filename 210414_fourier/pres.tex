\documentclass[9pt]{beamer}

\input{preambule}

\input{title}

\begin{document}

\begin{frame}[plain]
  \maketitle
\end{frame}

\begin{frame}{Fourier Neural Operator}
  Network based on the Neural Operator method, see \cite{li2020neural}, with parametrization directly in Fourier space.
  \begin{block}{Goals of the method}
    \begin{itemize}
      \item Learn mappings between function spaces
      \item Mesh invariant neural network 
      \item No need of prior knowledge on the PDEs
      \item Computationaly effective
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Context}
  We want to solve a PDE of the form:
  \begin{align*}
    (\mathcal{L}_a u)(x) &= f(x), &\quad x\in D\\
    u(x) &= 0, &\quad x\in \partial D
  \end{align*}
  with $u: D \rightarrow \mathbb{R}$ and parameter $a: D \rightarrow \mathbb{R}^{d_a}$.\\
  The domain $D$ is discretized into $K$ points and we have $N$ training pairs $(a_i,u_j)_{j=1}^N$.\\

  \vfill
  For example $D = [0,1]^d$ and:
  \begin{align*}
    -\divg (a(x)\grad u(x)) &= f(x), &\quad x\in D\\
    u(x) &= 0, &\quad x\in\partial D
  \end{align*}
\end{frame}

\begin{frame}{Mesh invariance}
  To be independant of the discretization, we lift the input in a higher dimensional space of dimension $d_v$
  \begin{equation*}
    a(x) \xrightarrow{P} v_0(x) \xrightarrow{G} v_T(x) \xrightarrow{Q} u(x)
  \end{equation*}
  \begin{itemize}
    \item $v_0(x) = P(a(x))$: P local transformation parametrized by a shallow fully connected neural network
    \item $G$: iterative approximation of the operator
    \item $u(x) = Q(v_T(x))$: projection of $v_T$ by a local transformation parametrized by a shallow fully connected neural network
  \end{itemize}
\end{frame}

\begin{frame}{Graph Kernel Network}
  We introduce the Green's function $G_a: D \times D \rightarrow \mathbb{R}$, solution of:
  \begin{equation*}
    \mathcal{L}_a G_a(x,\cdot) = \delta_x
  \end{equation*}
  then the solution $u$ can be written as:
  \begin{equation*}
    u(x) = \int_D G_a(x,y)f(y)dy
  \end{equation*}

  The idea is to use a neural network to model $G_a$.
\end{frame}

\begin{frame}{Iterative approximation}
  The Green's function will be approximated iteratively, from $v_0$ to $v_T$ by:
  \begin{equation*}
    v_{t+1}(x) = \sigma\left(Wv_t(x) + \int_D \kappa_\phi(x,y,a(x),a(y))v_t(y)\nu_x(dy)\right)
  \end{equation*}
  where:
  \begin{itemize}
    \item $\sigma$ is a non-linear activation function
    \item $W\in\mathbb{R}^{d_v\times d_v}$ is a linear transformation
    \item $\kappa_\phi: \mathbb{R}^{2(d+d_a)} \rightarrow \mathbb{R}^{d_v\times d_v}$ is a neural network parametrized by $\phi$.
  \end{itemize}
  \vfill
  With a discretization of $K$ points, $\kappa_\phi$ may be viewed as a $K\times K$ kernel block matrix, where each entry $\kappa_\phi(x,y)$ is itself a $d_v\times d_v$ matrix.
  Each block shares the same set of network parameters.
  This is the key to making a method that shares common parameters independent of the discretization used.
\end{frame}

\begin{frame}{Fourier Neural Operator}
  In \cite{li2021fourier}, they propose to remove the dependence on $a$ and impose $\kappa_\phi(x,y)=\kappa_\phi(x-y)$.\\
  Then:
  \begin{equation*}
    \left(\mathcal{K}(a;\phi)v_t\right)(x) = \int_D \kappa_\phi(x,y,a(x),a(y))v_t(y)dy \qquad\forall x\in D
  \end{equation*}
  is a convolution operator.\\
  \vfill
  To compute it efficiently, they propose to parametrize $\kappa_\phi$ directly in Fourier space and using the Fast Fourier Transform (FFT).
\end{frame}

\begin{frame}{Fourier Neural Operator}
  Let $\mathcal{F}$ denote the Fourier transform of a function $f : D \rightarrow \mathbb{R}^{d_v}$ and $\mathcal{F}^{-1}$ its inverse then
  \begin{equation*}
    (\mathcal{F}f)_j(k) = \int_D f_j(x)e^{-2i\pi\langle x,k\rangle} dx, \qquad (\mathcal{F}^{-1}f)_j(k) = \int_D f_j(k)e^{2i\pi\langle x,k\rangle} dk
  \end{equation*}
  for $j=1,\dots, d_v$.\\
  By using $\kappa_\phi(x,y) = \kappa_\phi(x-y)$ and the convolution theorem:
  \begin{equation*}
    \left(\mathcal{K}(a;\phi)v_t\right)(x) = \mathcal{F}^{-1}\left(\mathcal{F}(\kappa_\phi)\cdot\mathcal{F}(v_t)\right)(x), \quad \forall x\in D
  \end{equation*}
  \vfill
  The operator $\mathcal{F}(\kappa_\phi)$ can be directly parametrized in Fourier space:
  \begin{equation*}
    \left(\mathcal{K}(a;\phi)v_t\right)(x) = \mathcal{F}^{-1}\left(R_\phi\cdot\mathcal{F}(v_t)\right)(x), \quad \forall x\in D
  \end{equation*}
\end{frame}

\begin{frame}{Fourier Neural Operator}
  For frequency mode $k\in D$, we have $\left(\mathcal{F}v_t\right)(k)\in\mathbb{C}^{d_v}$ and $R_\phi(k)\in\mathbb{C}^{d_v\times d_v}$.\\
  Assuming $\kappa$ is periodic, we can truncate the Fourier series at a maximal number of modes $k_{max}$.\\
  $R_\phi$ is then directly parametrize as a complex-valued $(k_{max}\times d_v \times d_v)$ tensor and $\mathcal{F}(v_t)\in\mathbb{C}^{k_{max}\times d_v}$ leading to:
  \begin{equation*}
    \left(R_\phi\cdot(\mathcal{F}v_t)\right)_{k,l} = \sum_{j=1}^{d_v} R_{k,l,j}(\mathcal{F}v_t)_{k,j}, \qquad k=1,\dots,k_{max},\quad l=1,\dots,d_v
  \end{equation*}

  The computational cost lies in majority in computing the Fourier transform and its inverse.
  In general, computing the transform has complexity $O(K^2)$, but by truncating the modes, we can achieve $O(Kk_{max})$.\\
  If the discretization of $D$ is uniform, we can use the Fast Fourier Transform to compute $\mathcal{F}$ and $\mathcal{F}^{-1}$.
  The complexity is then $O(K \log K)$.  
\end{frame}

\begin{frame}{Concept}
  \includegraphics[width=\textwidth]{Figures/concept.png}
\end{frame}

\begin{frame}{Results}
  \begin{figure}
    \centering
    \includegraphics[width=0.55\textwidth]{Figures/result-burger.png}
    \caption{Benchmark on Burgers equation}
  \end{figure}
  FCN: Fully Convolution Networks. PCANN: operator method using PCA as an autoencoder. GNO: Graph Neural Operator. MGNO: Multipole Graph Neural Operator. LNO: neural operator method based on the low-rank decomposition of the kernel. FNO: Fourier Neural Operator.
\end{frame}

\begin{frame}{Results}
  \begin{figure}
    \centering
    \includegraphics[width=0.55\textwidth]{Figures/result-darcy.png}
    \caption{Benchmark on Darcy equation}
  \end{figure}
  FCN: Fully Convolution Networks. PCANN: operator method using PCA as an autoencoder. GNO: Graph Neural Operator. MGNO: Multipole Graph Neural Operator. LNO: neural operator method based on the low-rank decomposition of the kernel. FNO: Fourier Neural Operator.
\end{frame}

\begin{frame}{Results}
  \begin{figure}
    \centering
    \includegraphics[width=0.55\textwidth]{Figures/result-ns.png}
    \caption{Benchmark on Navier Stokes equation}
  \end{figure}
  \vspace{-1.5em}
  ResNet: 18 layers of 2-d convolution. U-Net: Four blocks with 2-d convolutions and deconvolutions. TF-Net: A network based on a combination of spatial and temporal convolution. FNO-2d: 2-d Fourier neural operator with a RNN structure in time. FNO-3d: 3-d Fourier neural operator that directly convolves in space-time.
\end{frame}

\begin{frame}{Results}
  \animategraphics[loop,controls,width=\linewidth]{10}{Figures/li-ns-}{0}{77}
\end{frame}

\begin{frame}{Code \cite{li2020github}}
  The code provided by the authors uses \href{https:pytorch.org}{PyTorch}, an open source machine learning framework in python.\\
  It needs CUDA to work (simpliest is to use atlas4).\\
  \vfill
  You just need to define each layer of your network and its overall architecture.
\end{frame}

\begin{frame}[fragile]{Code: Fourier layer in 1D \cite{li2020github}}
  \begin{lstlisting}[language=Python]
class SpectralConv1d(nn.Module):
  def __init__(self, in_channels, out_channels, modes1):
    super(SpectralConv1d, self).__init__()
    """
    1D Fourier layer. It does FFT, linear transform, and Inverse FFT.    
    """

    self.in_channels = in_channels
    self.out_channels = out_channels
    self.modes1 = modes1  #Number of Fourier modes to multiply, at most floor(N/2) + 1
    self.scale = (1 / (in_channels*out_channels))
    self.weights1 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, self.modes1, 2))

  def forward(self, x):
    batchsize = x.shape[0]
    #Compute Fourier coeffcients up to factor of e^(- something constant)
    x_ft = torch.rfft(x, 1, normalized=True, onesided=True)

    # Multiply relevant Fourier modes
    out_ft = torch.zeros(batchsize, self.in_channels, x.size(-1)//2 + 1, 2, device=x.device)
    out_ft[:, :, :self.modes1] = compl_mul1d(x_ft[:, :, :self.modes1], self.weights1)

    #Return to physical space
    x = torch.irfft(out_ft, 1, normalized=True, onesided=True, signal_sizes=(x.size(-1), ))
    return x
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Code: Network definition in 1D \cite{li2020github}}
  \begin{lstlisting}[language=Python]
class SimpleBlock1d(nn.Module):
  def __init__(self, modes, width):
    super(SimpleBlock1d, self).__init__()
    """
    The overall network. It contains 4 layers of the Fourier layer.
    1. Lift the input to the desire channel dimension by self.fc0 .
    2. 4 layers of the integral operators u' = (W + K)(u).
        W defined by self.w; K defined by self.conv .
    3. Project from the channel space to the output space by self.fc1 and self.fc2 .
    input: the solution of the initial condition and location (a(x), x)
    input shape: (batchsize, x=s, c=2)
    output: the solution of a later timestep
    output shape: (batchsize, x=s, c=1)
    """
    self.modes1 = modes
    self.width = width
    self.fc0 = nn.Linear(2, self.width) # input channel is 2: (a(x), x)

    self.conv0 = SpectralConv1d(self.width, self.width, self.modes1)
    self.conv1 = SpectralConv1d(self.width, self.width, self.modes1)
    self.conv2 = SpectralConv1d(self.width, self.width, self.modes1)
    self.conv3 = SpectralConv1d(self.width, self.width, self.modes1)
    self.w0 = nn.Conv1d(self.width, self.width, 1)
    self.w1 = nn.Conv1d(self.width, self.width, 1)
    self.w2 = nn.Conv1d(self.width, self.width, 1)
    self.w3 = nn.Conv1d(self.width, self.width, 1)

    self.fc1 = nn.Linear(self.width, 128)
    self.fc2 = nn.Linear(128, 1)
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Code: Network in 1D \cite{li2020github}}
  \begin{lstlisting}[language=Python]
def forward(self, x):
  x = self.fc0(x)
  x = x.permute(0, 2, 1)

  x1 = self.conv0(x)
  x2 = self.w0(x)
  x = x1 + x2
  x = F.relu(x)

  x1 = self.conv1(x)
  x2 = self.w1(x)
  x = x1 + x2
  x = F.relu(x)

  x1 = self.conv2(x)
  x2 = self.w2(x)
  x = x1 + x2
  x = F.relu(x)

  x1 = self.conv3(x)
  x2 = self.w3(x)
  x = x1 + x2

  x = x.permute(0, 2, 1)
  x = self.fc1(x)
  x = F.relu(x)
  x = self.fc2(x)
  return x
  \end{lstlisting}
\end{frame}

\begin{frame}{Conclusion}
  \begin{itemize}
    \item Better error than the other methods used in the benchmark
    \item Resolution invariant, same error at different resolutions
    \item Zero shot super resolution 
    \item Complexity $O(Kk_{max})$ or $O(K \log K)$ with FFT (but needs uniform discretization)
  \end{itemize}
\end{frame}

\input{annexe}

\end{document}
