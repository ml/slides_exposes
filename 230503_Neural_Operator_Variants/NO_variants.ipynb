{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Neural Operators – Variants"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 1/ Low-rank Neural Operator (LRNO)\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [Kovachki, Li, Liu, Azizzadenesheli, Bhattacharya, Stuart, Anandkumar, 2022]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: Reduce computational complexity by writing the kernel $k$ under the form of a tensor product."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Recall that the output $u: D \\to \\mathbb{R}^m$ of a NO is under the form \n",
    " \n",
    "$$\n",
    "    u(x) = (\\kappa \\ast v)(x) = \\int_D \\kappa(x, y) v(y) dy,\n",
    "$$\n",
    "\n",
    "with $\\kappa: D \\times D \\to \\mathcal{M}_{m, n}(\\mathbb{R})$ a kernel function and $v: D \\to \\mathbb{R}^n$ the input function.\n",
    "\n",
    "It can be useful to consider the kernel matrix associated to $\\kappa$. \n",
    "To that end, we take $N$ evaluation points $x_i \\in D$.\n",
    "Then the kernel matrix $K \\in \\mathcal{M}_{mN, nN}(\\mathbb{R})$ is a $N \\times N$ block matrix, with each block given by the value of the kernel at the evaluation point under consideration:\n",
    "\n",
    "$$\n",
    "    \\forall i, j \\in \\{ 1, \\ldots, N \\}, \\quad\\quad K_{ij} = \\kappa(x_i, x_j) \\quad \\in \\mathcal{M}_{m, n}(\\mathbb{R}).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "For simplicity, we present the forthcoming developments for $m = n = 1$, i.e. for $u$ and $v$ with values in $\\mathbb{R}$.\n",
    "\n",
    "A way to reduce the computational complexity is to take $R \\ll N$, and write $\\kappa$ under the form of a tensor product:\n",
    "\n",
    "$$\n",
    "    \\kappa(x, y) = \\sum_{r=1}^R \\varphi^{(r)}(x) \\psi^{(r)}(y),\n",
    "$$\n",
    "\n",
    "where the functions $(\\varphi^{(r)})_{1 \\leq r \\leq R}: D \\to \\mathbb{R}$ and $(\\psi^{(r)})_{1 \\leq r \\leq R}: D \\to \\mathbb{R}$ are either the components of two neural networks $\\varphi: D \\to \\mathbb{R}^R$ and $\\psi: D \\to \\mathbb{R}^R$, or the result of a single neural network $\\Xi: D \\to \\mathbb{R}^{2R}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Using this expression of $\\kappa$, the output of the neural operator rewrites:\n",
    "\n",
    "\\begin{align}\n",
    "    u(x) & = \\int_D \\kappa(x, y) v(y) dy \\\\\n",
    "         & = \\int_D \\sum_{r=1}^R \\varphi^{(r)}(x) \\psi^{(r)}(y) v(y) dy \\\\\n",
    "         & = \\sum_{r=1}^R \\left( \\int_D \\psi^{(r)}(y) v(y) dy \\right) \\varphi^{(r)}(x) \\\\\n",
    "         & = \\sum_{r=1}^R \\left\\langle \\psi^{(r)}, v \\right\\rangle_{L^2(D, \\mathbb{R})} \\varphi^{(r)}(x).\n",
    "\\end{align}\n",
    "\n",
    "The inner products can be pre-computed, and are independent of the evaluation point $x$.\n",
    "Therefore, the computational complexity of evaluating $u$ is $\\mathcal{O}(RN)$, compared to $\\mathcal{O}(N^2)$ for usual neural operators."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Note that this procedure imposes a specific low-rank structure for the kernel matrix. \n",
    "Indeed, we can write $K = \\Phi \\Psi$, where $\\Phi \\in \\mathcal{M}_{N, R}(\\mathbb{R})$ and $\\Psi \\in \\mathcal{M}_{R, N}(\\mathbb{R})$ are defined such that \n",
    "\n",
    "$$\n",
    "    \\Phi_{i r} = \\varphi^{(r)}(x_i)\n",
    "    \\quad \\text{ and } \\quad\n",
    "    \\Psi_{r j} = \\psi^{(r)}(x_j),\n",
    "$$\n",
    "\n",
    "so that \n",
    "\n",
    "$$ \n",
    "    K_{ij} = \\sum_{r=1}^R \\Phi_{i r} \\Psi_{r j} = \\sum_{r=1}^R \\varphi^{(r)}(x_i) \\psi^{(r)}(x_j).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Going back to $m \\neq 1$ and $n \\neq 1$, we write $u$ as a tensor product:\n",
    "\n",
    "\\begin{align}\n",
    "    u & = \\sum_{r=1}^R \\left( \\varphi^{(r)} \\otimes \\psi^{(r)} \\right) v \\\\\n",
    "      & = \\sum_{r=1}^R \\left\\langle \\psi^{(r)}, v \\right\\rangle_{L^2(D, \\mathbb{R}^n)} \\varphi^{(r)},\n",
    "\\end{align}\n",
    "\n",
    "and we see that the low-rank structure on the kernel matrix is preserved, as well as the low computational complexity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2/ Wavelet Neural Operator (WNO)\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [Tripura, Chakraborty, 2022]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: Use a wavelet transform of the kernel to handle highly nonlinear PDEs with discontinuities and sharp gradients."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"WNO_1.png\" width=\"1200\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"WNO_2.png\" width=\"1200\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "For the WNO, we replace the convolution with kernel $\\kappa$ by a convolution in the wavelet decomposition.\n",
    "\n",
    "To that end, consider a mother wavelet $\\psi \\in L^2(\\mathbb{R})$, for instance \n",
    "\n",
    "$$\n",
    "    \\psi(x) = \\frac{\\sin(2 \\pi x) - \\sin(\\pi x)}{\\pi x}.\n",
    "$$\n",
    "\n",
    "This function $\\psi$ is defined so that its Fourier transform $\\hat \\psi$ satisfies\n",
    "\n",
    "$$\n",
    "    C_\\psi = \\int_{-\\infty}^{+\\infty} \\frac { |\\hat \\psi(\\omega)|^2 }{ |\\omega| } d\\omega < + \\infty.\n",
    "$$\n",
    "\n",
    "Child wavelets are computed from the mother wavelet by a scaling with parameter $\\alpha \\in \\mathbb{R}^*_+$ and a translation with parameter $\\beta \\in \\mathbb{R}$:\n",
    "\n",
    "$$\n",
    "    \\psi_{\\alpha, \\beta}(x) = \\frac 1 {\\sqrt \\alpha} \\psi \\left( \\frac {x - \\beta} \\alpha \\right).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Then, the continuous forward and inverse wavelet transforms of some function $\\Gamma : D \\to \\mathbb{R}^n$ are denoted by $\\mathcal{W}(\\Gamma)$ and $\\mathcal{W}^{-1}(\\Gamma)$, and are given by:\n",
    "\n",
    "$$\n",
    "    \\mathcal{W}(\\Gamma)(\\alpha, \\beta) = \\left\\langle \\Gamma, \\psi_{\\alpha, \\beta} \\right\\rangle\n",
    "$$\n",
    "\n",
    "and \n",
    "\n",
    "$$\n",
    "    \\mathcal{W}^{-1}(\\Gamma)(x) = \\frac 1 {C(\\psi)} \\int_{\\mathbb{R}^*_+} \\left[ \\int_{\\mathbb{R}} \\bigl( \\mathcal{W}(\\Gamma)(\\alpha, \\beta) \\, \\psi_{\\alpha, \\beta}(x) \\bigr) d\\beta \\right] \\frac{d\\alpha}{\\alpha^2}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We can prove that \n",
    "\n",
    "$$\n",
    "    u = \\kappa \\ast v = \\mathcal{W}^{-1} \\bigl( \\mathcal{W}(\\kappa) \\cdot \\mathcal{W}(v) \\bigr),\n",
    "$$\n",
    "\n",
    "and so, instead of learning $\\kappa$, one can directly learn its wavelet transform, denoted by $\\mathcal{K} := \\mathcal{W}(\\kappa)$, to get \n",
    "\n",
    "$$\n",
    "    u = \\mathcal{W}^{-1} \\bigl( \\mathcal{K} \\cdot \\mathcal{W}(v) \\bigr).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In practice, to save computation time, we do not perform continuous wavelet transforms, but rather discrete ones, by taking $m \\in \\mathbb{Z}^+$ and $\\tau \\in \\mathbb{Z}_+^*$ such that\n",
    "\n",
    "$$\n",
    "    \\psi_{m, \\tau}(x) = \\frac 1 {\\sqrt {2^m}} \\psi \\left( \\frac {x - \\tau 2^m} {2^m} \\right).\n",
    "$$\n",
    "\n",
    "In this case, we can reduce the computational complexity of the algorithm: instead of computing a $N \\times N$ convolution $\\kappa \\ast v$, for a chosen $m$, we obtain a $(N / 2^m) \\times (N / 2^m)$ pointwise multiplication $\\mathcal{K} \\cdot \\mathcal{W}(v)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 3/ Spectral Neural Operator (SNO)\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [Fanaskov, Oseledets, 2022]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: Avoid the aliasing problem inherent to FNO by decoupling interpolation from the mapping between function spaces."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "It can be shown that FNO suffer from an _aliasing_ problem, \n",
    "i.e. higher frequencies can be clipped and become indistinguishable from lower frequencies.\n",
    "\n",
    "To avoid this, the authors propose to decouple the interpolation from the mapping between function spaces.\n",
    "\n",
    "To that end, the interpolation is first done on a space spanned by trigonometric or Chebyshev polynomials."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Trigonometric polynomials are the functions $\\theta_k(x) = e^{\\text{i} \\pi k x}$.\n",
    "\n",
    "Recall that Chebyshev polynomials are defined by recurrence:\n",
    "\n",
    "\\begin{align*}\n",
    "    T_0(x) &= 1, \\\\\n",
    "    T_1(x) &= x, \\\\\n",
    "    T_k(x) &= 2 x T_{k-1}(x) - T_{k-2}(x).\n",
    "\\end{align*}\n",
    "\n",
    "In what follows, we denote by $f_k(x)$ either Chebyshev or trigonometric polynomials."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Projecting on bases made of such polynomials have good approximation properties:\n",
    "- efficient compression of smooth functions,\n",
    "- efficient computation of the coefficients of the projection,\n",
    "- many operations (such as differentiation, for instance) can be computed with machine accuracy,\n",
    "- the projection does not require a uniform grid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Then, the SNO has the following structure.\n",
    "\n",
    "$$\n",
    "    u \\xrightarrow{\\mathcal{E}} f \\xrightarrow{\\mathcal{K}} g \\xrightarrow{\\mathcal{D}} v,\n",
    "$$\n",
    "\n",
    "where $u$ is first encoded by $\\mathcal{E}$ in the space spanned by the functions $f_k$, \n",
    "then the neural operator $\\mathcal{K}$ is applied, before its output is decoded by $\\mathcal{D}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 4/ Laplacian Neural Operator for complex geometries (LNO)\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [Chen, Liu, Li, Meng, Chen, 2023]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: To handle manifolds, replace the Fourier basis in the FNO with the basis of eigenfunctions of the Laplace-Beltrami operator on the manifold. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "For a FNO, the solution is approximated with \n",
    "\n",
    "$$\n",
    "    u = \\kappa \\ast v = \\mathcal{F}^{-1} \\bigl( \\mathcal{F}(\\kappa) \\cdot \\mathcal{F}(v) \\bigr),\n",
    "$$\n",
    "\n",
    "where $\\mathcal{F}$ and $\\mathcal{F}^{-1}$ are the forward and inverse Fourier transforms. \n",
    "Then, the Fourier transform $\\mathcal{K} := \\mathcal{F}(\\kappa)$ is directly learned.\n",
    "Keeping only the first $k$ modes of the Fourier transform,\n",
    "$\\mathcal{K}$ turns out to be of size $k \\times m \\times n$.\n",
    "\n",
    "The Fourier transform is nothing but a projection on the orthogonal basis made of the functions \n",
    "$(\\psi_k)_{k \\in \\mathbb{Z}}$, where $\\psi_k(x) = e^{2 \\text{i} \\pi k x}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Now, note that the functions $\\psi_k$ are eigenfunctions of the Laplacian defined on $\\mathbb{R}^d$,\n",
    "with eigenvalues $-(2 \\pi k)^2$, i.e. they verify $\\Delta \\psi_k = k \\psi_k$.\n",
    "\n",
    "Consider now a manifold $\\mathcal{V} \\subset \\mathbb{R}^d$.\n",
    "The idea behind the Laplacian neural operator is to replace these functions $\\psi_k$,\n",
    "eigenfunctions of the Laplacian on $\\mathbb{R}^d$, by the functions $\\varphi_k$, \n",
    "which are the eigenfunctions of the Laplace-Beltrami operator on $\\mathcal{V}$.\n",
    "\n",
    "For the LNO, $\\mathcal{K}$ is set to $\\mathcal{K} = \\mathcal{D} \\, \\mathcal{L} \\, \\mathcal{E}$, \n",
    "where $\\mathcal{E}$ and $\\mathcal{D}$ are, respectively,\n",
    "an encoder and a decoder to project to and from the space spanned by the $\\varphi_k$, \n",
    "truncated at some index $k$.\n",
    "We get the following diagram, which shows that $\\mathcal{L}$ is also of size $k \\times m \\times n$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "$$\n",
    "\\begin{array}{ccc}\n",
    "    v \\in L^2(D, \\mathbb{R}^m) & \\to & u \\in L^2(D, \\mathbb{R}^n) \\\\\n",
    "    & & \\\\\n",
    "    \\downarrow {\\small \\mathcal{E}} & & \\uparrow {\\small \\mathcal{D}} \\\\\n",
    "    & & \\\\\n",
    "    \\mathcal{M}_{k, m}(\\mathbb{R}) & \\xrightarrow{\\mathcal{L}} & \\mathcal{M}_{k, n}(\\mathbb{R})\n",
    "\\end{array}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 5/ Laplace Transform Neural Operator (LTNO)\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [Cao, Goswami, Karniadakis, 2023]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: Deal with non-periodic data and provide a better handling of initial conditions by using the Laplace transform instead of the Fourier transform in a FNO."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The Laplace transform of some function $\\Gamma$ is given for $s = \\sigma + \\text{i} \\omega \\in \\mathbb{C}$ by \n",
    "\n",
    "$$\n",
    "    \\mathcal{L}(\\Gamma)(s) = \\int_{0}^{+ \\infty} \\Gamma(t) e^{-s t} dt,\n",
    "$$\n",
    "\n",
    "while its Fourier transform is given by \n",
    "\n",
    "$$\n",
    "    \\mathcal{F}(\\Gamma)(\\omega) = \\int_{0}^{+ \\infty} \\Gamma(t) e^{-\\text{i} \\omega t} dt,\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The Laplace transform of the derivatives of $\\Gamma$ still contain some information on the initial conditions,\n",
    "contrary to the Fourier transform:\n",
    "\n",
    "$$\n",
    "    \\begin{array}{ccc}\n",
    "        \\mathcal{L}(\\Gamma')(s) = s \\mathcal{L}(\\Gamma)(s) - \\Gamma(0), & &\n",
    "        \\mathcal{F}(\\Gamma')(\\omega) = \\text{i} \\omega \\mathcal{F}(\\Gamma)(\\omega), \\\\\n",
    "        \\mathcal{L}(\\Gamma'')(s) = s^2 \\mathcal{L}(\\Gamma)(s) - s\\Gamma(0) - \\Gamma'(0), & & \n",
    "        \\mathcal{F}(\\Gamma'')(\\omega) = - \\omega^2 \\mathcal{F}(\\Gamma)(\\omega).\n",
    "    \\end{array}\n",
    "$$\n",
    "\n",
    "This makes it a more suitable candidate for learning solutions for different initial conditions,\n",
    "and for the approximation of non-periodic data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "To use it, we take the Laplace transform of the relation $u = \\kappa \\ast v$, to get \n",
    "\n",
    "$$\n",
    "    U(s) := \\mathcal{L}(u)(s) = \\mathcal{L}(\\kappa \\ast v)(s) = \\mathcal{L}(\\kappa)(s) \\mathcal{L}(v)(s) =: \\mathcal{K}(s) V(s).\n",
    "$$\n",
    "\n",
    "We express the Laplace transform of the kernel under the pole-residue form:\n",
    "\n",
    "$$\n",
    "    \\mathcal{K}(s) = \\sum_{n=1}^N \\frac{\\beta_n}{s - \\mu_n},\n",
    "$$\n",
    "\n",
    "where the $\\beta_n$ and the $\\mu_n$ are the trainable parameters (resides and poles) of the network."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Now, taking a periodic $v$, it can be decomposed into its Fourier series, with $\\alpha_\\ell \\in \\mathbb{C}$:\n",
    "\n",
    "$$\n",
    "    v(t) = \\sum_{\\ell = -\\infty}^{+ \\infty} \\alpha_\\ell e^{\\text{i} \\omega_\\ell t}.\n",
    "$$\n",
    "\n",
    "Its Laplace transform reads\n",
    "\n",
    "$$\n",
    "    V(s) = \\sum_{\\ell = -\\infty}^{+ \\infty} \\frac{\\alpha_\\ell}{s - \\text{i} \\omega_\\ell}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We get, for $U$:\n",
    "\n",
    "$$\n",
    "    U(s)\n",
    "    = \n",
    "    \\left( \\sum_{n=1}^N \\frac{\\beta_n}{s - \\mu_n} \\right)\n",
    "    \\left( \\sum_{\\ell = -\\infty}^{+ \\infty} \\frac{\\alpha_\\ell}{s - \\text{i} \\omega_\\ell} \\right)\n",
    "    =\n",
    "    \\left( \\sum_{n=1}^N \\frac{\\gamma_n}{s - \\mu_n} \\right) +\n",
    "    \\left( \\sum_{\\ell = -\\infty}^{+ \\infty} \\frac{\\lambda_\\ell}{s - \\text{i} \\omega_\\ell} \\right).\n",
    "$$\n",
    "\n",
    "The coefficients $\\gamma_n$ and $\\lambda_\\ell$ can be obtained by the residue theorem:\n",
    "\n",
    "$$\n",
    "    \\gamma_n = \\beta_n V(\\mu_n)\n",
    "    \\quad \\text{ and } \\quad \n",
    "    \\lambda_\\ell = \\alpha_\\ell \\mathcal{K}(\\text{i} \\omega_\\ell).\n",
    "$$\n",
    "\n",
    "Once $U$ has been computed, we take its inverse Laplace transform $\\mathcal{L}^{-1}$ to recover $u$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Finally, the key difference between FNO and LTNO is:\n",
    "\n",
    "- for the FNO, $\\mathcal{K}$ is parameterized by $(\\mathcal{K}(\\text{i} \\omega_1), \\ldots, \\mathcal{K}(\\text{i} \\omega_L))$ in the frequency domain;\n",
    "- while for the LNO, $\\mathcal{K}$ is parameterized by $(\\beta_1, \\ldots, \\beta_N, \\mu_1, \\ldots, \\mu_N)$ as residues and poles in the Laplace transform.\n",
    "\n",
    "From the numerical experiments, it seems that $N$ can be significantly lower than $L$ to still get a suitable approximation."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  },
  "vscode": {
   "interpreter": {
    "hash": "0adcc2737ebf6a4a119f135174df96668767fca1ef1112612db5ecadf2b6d608"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
