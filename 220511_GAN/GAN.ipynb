{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Les GAN (Generative Adversarial Networks)\n",
    "\n",
    "### Nicolas Juillet et Victor Michel-Dansac\n",
    "\n",
    "### 18/05/2022"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Les GAN ont été, à la base, développés pour générer des images.\n",
    "\n",
    "Un exemple bien connu est [this person does not exist](https://thispersondoesnotexist.com/), qui génère des images de personnes n'existant pas.\n",
    "Il existe une variante avec des chats : [this cat does not exist](https://thiscatdoesnotexist.com/).\n",
    "\n",
    "<table><tr>\n",
    "<td> <div><img src=\"img/fake_cat_1.jpg\" width=\"300\"/></div> </td>\n",
    "<td> <div><img src=\"img/fake_cat_2.jpg\" width=\"300\"/></div> </td>\n",
    "</tr></table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Le fonctionnement des GAN est basé sur un jeu à somme nulle (*Adversarial*) entre deux réseaux de neurones (*Network*), le premier générant les images (*Generative*), et le deuxième discriminant les images générées contre les images de la base de données.\n",
    "\n",
    "Quelques références sur les GAN :\n",
    "\n",
    "- [livre \"Probabilistic Machine Learning\"](https://probml.github.io/pml-book/book2.html)\n",
    "- [article original sur les Wasserstein-GAN](https://arxiv.org/pdf/1701.07875.pdf)\n",
    "- [article de blog sur les GAN et les Wasserstein-GAN](https://lilianweng.github.io/posts/2017-08-20-gan/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1. Principe "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "On se donne une base de données de photos de chats.\n",
    "On va supposer que les photos de chats suivent une loi $p^*$ inconnue et à déterminer : avec $x$ une image, on va dire que $x \\sim p^*$ si c'est une photo de chat de la base de données.\n",
    "\n",
    "On n'a donc accès qu'à des réalisations de cette loi $p^*$, i.e. les photos de chats de notre base de données, et on souhaite construire de nouvelles photos de chats, c'est-à-dire approcher $p^*$ par une certaine loi $q$ *seulement à travers leurs réalisations*.\n",
    "**La loi $q$ sera la loi du générateur d'images.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Puisqu'on doit générer des images, donc générer des réalisations de la loi $q$, on va se placer dans le cadre des *modèles probabilistes implicites*. Ces derniers construisent un simulateur pour générer des réalisations d'une certaine loi, contrairement aux *modèles probabilistes explicites*, qui permettent d'apprendre une densité.\n",
    "\n",
    "Ici, la loi n'aura pas de densité.\n",
    "\n",
    "<center><div><img src=\"img/explicit_vs_implicit.png\" width=\"1000\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Ici, ce qu'on souhaiterait faire, c'est calculer le maximum de vraisemblance entre la vraie loi $p^*$ et la loi $q$ des images générées.\n",
    "Or, maximiser la vraisemblance revient à minimiser la divergence de Kullback-Leibler $KL$.\n",
    "\n",
    "En effet, avec $N$ tirages $x_i$ suivant la loi $p^*$, la log-vraisemblance $L$ sera donnée par \n",
    "$$\n",
    "L(q, N) = \\log \\left( \\prod_{i=1}^n q(x_i) \\right) = \\sum_{i=1}^n \\log \\left( q(x_i) \\right).\n",
    "$$\n",
    "La loi des grands nombres nous dit de plus que \n",
    "$$\n",
    "\\lim_{N \\to +\\infty} L(q, N) = \\lim_{N \\to +\\infty} \\frac 1 N \\sum_{i=1}^n \\log \\left( q(x_i) \\right) = \\mathbb{E}_{x \\sim p^*} [\\log(q(x))].\n",
    "$$\n",
    "\n",
    "Or la divergence de Kullback-Leibler entre $p^*$ et $q$ s'écrit :\n",
    "\n",
    "$$\n",
    "KL(p^* \\mid\\mid q) = \\mathbb{E}_{x \\sim p^*} \\left[ \\log \\left( \\frac{p^*(x)}{q(x)} \\right) \\right]\n",
    "                          = \\mathbb{E}_{x \\sim p^*} \\left[ \\log \\left( p^*(x) \\right) \\right] - \\mathbb{E}_{x \\sim p^*} \\left[ \\log \\left( q(x) \\right) \\right]\n",
    "$$\n",
    "\n",
    "Donc minimiser la divergence de KL revient à maximiser $\\mathbb{E}_{x \\sim p^*} \\left[ \\log \\left( q(x) \\right) \\right]$, qui se trouve être la valeur de la limite quand $N$ tend vers l'infini de la log-vraisemblance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Or, dans notre cas des modèles implicites, on ne peut pas calculer la divergence de KL car on n'a pas accès aux lois $p^*$ et $q$, seulement à des réalisations !\n",
    "\n",
    "**Idée** : remplacer la divergence de KL par une certaine fonction $\\mathcal{D}(p^*, q)$ avec de bonnes propriétés :\n",
    "1. $\\arg \\min_q \\mathcal{D}(p^*, q) = p^*$, pour que $q$ approche bien $p^*$ ;\n",
    "2. $\\mathcal{D}$ peut être évaluée seulement avec des réalisations des lois $p^*$ et $q$ ;\n",
    "3. $\\mathcal{D}$ n'est pas trop chère à calculer.\n",
    "\n",
    "Les distances classiques (KL, Jensen-Shannon, Wasserstein) vérifient bien 1., mais pas les deux autres propriétés : par exemple, KL ne peut pas être évaluée simplement au travers de réalisations, et Wasserstein est très coûteuse."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Plutôt que d'utiliser une formule explicite pour $\\mathcal{D}(p^*, q)$, on va introduire le **discriminateur** $D$ tel que:\n",
    "\n",
    "$$\n",
    "\\mathcal{D}(p^*, q) = \\arg\\max_D \\left( \\mathcal{F}(D, p^*, q) \\right),\n",
    "$$\n",
    "\n",
    "où $\\mathcal{F}$ est une fonction qui ne dépend de $p^*$ et $q$ qu'à travers leurs réalisations, précisée plus tard."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "En pratique, le générateur (de loi $q$) et le discriminateur $D$ seront des réseaux de neurones.\n",
    "On note leurs paramètres respectifs $\\theta$ et $\\varphi$, et on note donc $q_\\theta$ la loi du générateur et $D_\\varphi$ le discriminateur.\n",
    "\n",
    "Le discriminateur va donc être solution du problème d'optimisation suivant (sur l'ensemble des paramètres plutôt que sur l'ensemble des fonctions) :\n",
    "\n",
    "$$\n",
    "\\mathcal{D}(p^*, q_\\theta) = \\arg\\max_\\varphi \\left( \\mathcal{F}(D_\\varphi, p^*, q_\\theta) \\right).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Revenons maintenant à la fonction $\\mathcal{F}$. Une bonne façon de s'assurer qu'elle ne dépend de $p^*$ et $q$ qu'à travers leurs réalisations est de dépendre de ces lois en espérance seulement.\n",
    "Ensuite, utiliser des méthodes de Monte-Carlo permettront d'approcher ces espérances uniquement avec des réalisations des lois.\n",
    "\n",
    "On se donne deux fonctions $f$ et $g$ (à déterminer plus tard) et on définit $\\mathcal{F}$ par la formule suivante, très générale :\n",
    "\n",
    "$$\n",
    "\\mathcal{F}(D_\\varphi, p^*, q) = \\mathbb{E}_{x \\sim p^*} \\left[ f(x, \\varphi) \\right] + \\mathbb{E}_{x \\sim q_\\theta} \\left[ g(x, \\varphi) \\right].\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "En pratique, le générateur utilise un espace latent de petite dimension pour générer ses données : les variables latentes $z$ y sont tirées suivant une loi $q$ (par exemple, un bruit gaussien) puis transformées par la fonction $G_\\theta$ qui représente le générateur.\n",
    "\n",
    "On aura donc $x \\sim q_\\theta \\iff (x = G_\\theta(z) \\text{ et } z \\sim q)$.\n",
    "\n",
    "<center><div><img src=\"img/latent_space.png\" width=\"700\"/></div></center>\n",
    "\n",
    "<div style=\"font-size: 10px\">\n",
    "    source : https://medium.com/analytics-vidhya/from-gan-basic-to-stylegan2-680add7abe82\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On a donc la suite d'égalités suivantes sur $\\mathcal{F}$ :\n",
    "\n",
    "\\begin{align*}\n",
    "    \\mathcal{F}(D_\\varphi, p^*, q) &= \\mathbb{E}_{x \\sim p^*} \\left[ f(x, \\varphi) \\right] + \\mathbb{E}_{x \\sim q_\\theta} \\left[ g(x, \\varphi) \\right], \\vphantom{\\dfrac 1 2} \\\\\n",
    "                                   &= \\mathbb{E}_{x \\sim p^*} \\left[ f(x, \\varphi) \\right] + \\mathbb{E}_{z \\sim q} \\left[ g(G_\\theta(z), \\varphi) \\right], \\vphantom{\\dfrac 1 2} \\\\\n",
    "                                   &\\simeq \\frac 1 N \\sum_{i=1}^N \\left[ f(\\hat x_i, \\varphi) \\right] + \\frac 1 N \\sum_{i=1}^N \\left[ g(G_\\theta(\\hat z_i), \\varphi) \\right].\n",
    "\\end{align*}\n",
    "\n",
    "**Prochaine étape** : que choisir pour $f$ et $g$ ?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2. Lien avec la divergence de Jensen-Shannon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Une façon de comparer deux lois à densité est de calculer le rapport de leurs densités : si ce rapport est égal à $1$, les lois seront les mêmes. Or, ici, les lois $p^*$ et $q_\\theta$ n'ont pas de densité.\n",
    "\n",
    "On va donc chercher à réécrire le rapport des densités d'une autre façon, et s'en servir ensuite pour proposer des fonctions $f$ et $g$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**On suppose dans cette partie que les lois ont en fait des densités** pour pouvoir faire les calculs, et voir le rapport avec la divergence de KL, mais en pratique ce ne sera pas le cas.\n",
    "\n",
    "On aura tout de même pu proposer des fonctions $f$ et $g$, qui nous serviront à calculer $\\mathcal{F}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Pour cela, on va réécrire le problème d'estimation de rapport de densités en un problème de classification binaire. \n",
    "On assigne le label $y = 1$ aux images de la base de données, i.e. pour $x \\sim p^*$, et $y = 0$ aux images générées, i.e. pour $x \\sim q_\\theta$.\n",
    "\n",
    "La densité de $p^*$ est alors $\\mathbb{P}(x \\mid y = 1)$ et celle de $q_\\theta$ est $\\mathbb{P}(x \\mid y = 0)$.\n",
    "\n",
    "Le rapport des densités vérifie donc :\n",
    "\n",
    "$$\n",
    "\\frac {\\mathbb{P}(x \\mid y = 1)} {\\mathbb{P}(x \\mid y = 0)} = \\frac{ \\dfrac{\\mathbb{P}(y = 1 \\mid x) \\, \\mathbb{P}(x)} {\\mathbb{P}(y = 1)} } { \\dfrac{\\mathbb{P}(y = 0 \\mid x) \\, \\mathbb{P}(x)} {\\mathbb{P}(y = 0)} }\n",
    "                                                            = \\frac{ \\mathbb{P}(y = 1 \\mid x) } { \\mathbb{P}(y = 0 \\mid x) }\n",
    "                                                            = \\frac{ D^*(x) } { 1 - D^*(x) }.\n",
    "$$\n",
    "\n",
    "On a supposé que $\\mathbb{P}(y = 0) = \\mathbb{P}(y = 1) = 0.5$, i.e. qu'autant de données et d'images générées sont utilisées pour entraîner $D$.\n",
    "\n",
    "De plus, on a noté $D^*(x) = \\mathbb{P}(y = 1 \\mid x)$ : $D^*$ est le classificateur parfait, qui ne se trompe jamais, et assigne toujours le bon label. Par exemple, connaissant une image $x$, $\\mathbb{P}(y = 1 \\mid x)$ est la proba que ce soit un chat, donc c'est ce qu'on veut que $D^*$ calcule."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On va donc apprendre les paramètres $\\varphi$ d'un discriminateur $D_\\varphi$, qui doit se rapprocher du discriminateur parfait $D^*$.\n",
    "Pour cela, on utilise l'entropie croisée binaire : on va chercher\n",
    "\n",
    "$$\n",
    "\\max_\\varphi V_\\varphi = \\max_\\varphi \\mathbb{E}_{\\mathbb{P}(x \\mid y) \\mathbb{P}(y)} \\left[ y \\log(D_\\varphi(x)) + (1 - y) \\log(1 - D_\\varphi(x)) \\right].\n",
    "$$\n",
    "\n",
    "Maximiser $V_\\varphi$ assure les propriétés suivantes sur $D_\\varphi(x) \\in (0, 1)$ :\n",
    "- $D_\\varphi(x)$ est maximal quand $y = 1$,\n",
    "- $1 - D_\\varphi(x)$ est maximal quand $y = 0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On peut de plus réécrire $V_\\varphi$ de la façon suivante :\n",
    "\n",
    "\\begin{align*}\n",
    "V_\\varphi &= \\mathbb{E}_{\\mathbb{P}(x \\mid y) \\mathbb{P}(y)} \\left[ y \\log(D_\\varphi(x)) + (1 - y) \\log(1 - D_\\varphi(x)) \\right] \\vphantom{\\dfrac 1 2}, \\\\\n",
    "          &= \\mathbb{E}_{\\mathbb{P}(x \\mid y = 1) \\mathbb{P}(y = 1)} \\left[ y \\log(D_\\varphi(x)) \\right] + \\mathbb{E}_{\\mathbb{P}(x \\mid y = 0) \\mathbb{P}(y = 0)} \\left[ (1 - y) \\log(1 - D_\\varphi(x)) \\right] \\vphantom{\\dfrac 1 2}, \\\\\n",
    "          &= \\frac 1 2 \\mathbb{E}_{x \\sim p^*} \\left[ \\log(D_\\varphi(x)) \\right] + \\frac 1 2 \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\log(1 - D_\\varphi(x)) \\right], \\\\\n",
    "\\end{align*}\n",
    "\n",
    "où on a utilisé $\\mathbb{P}(y = 0) = \\mathbb{P}(y = 1) = 0.5$, $p^*(x) = \\mathbb{P}(x \\mid y = 1)$ et $q_\\theta(x) = \\mathbb{P}(x \\mid y = 0)$.\n",
    "\n",
    "On a donc obtenu les fonctions inconnues $f$ et $g$ : on a $f(x, \\varphi) = \\log(D_\\varphi(x))$ et $g(x, \\varphi) = - \\log(1 - D_\\varphi(x))$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Maintenant, rappelons-nous que le discriminateur parfait $D^*$ vérifie \n",
    "$$\n",
    "D^* = \\frac{p^*}{p^* + q_\\theta},\n",
    "$$\n",
    "et donc le maximum $V^*$ de $V_\\varphi$ est atteint pour $D_\\varphi = D^*$.\n",
    "\n",
    "Il vérifie donc :\n",
    "\n",
    "\\begin{align*}\n",
    "V^* &= \\frac 1 2 \\mathbb{E}_{x \\sim p^*} \\left[ \\log \\left( \\frac{p^*}{p^* + q_\\theta} \\right) \\right] + \\frac 1 2 \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\log \\left( \\frac{q_\\theta}{p^* + q_\\theta} \\right) \\right], \\\\\n",
    "    &= \\frac 1 2 \\mathbb{E}_{x \\sim p^*} \\left[ \\log \\left( \\frac{p^*}{\\frac{p^* + q_\\theta}2} \\right) \\right] + \\frac 1 2 \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\log \\left( \\frac{q_\\theta}{\\frac{p^* + q_\\theta}2} \\right) \\right] - \\log(2), \\\\\n",
    "    &= \\frac 1 2 KL \\left( p^* \\left\\| \\frac{p^* + q_\\theta}2 \\right. \\right) + \\frac 1 2 KL \\left( q_\\theta \\left\\| \\frac{p^* + q_\\theta}2 \\right. \\right) - \\log(2), \\\\\n",
    "    &:= JS \\left( p^*, q_\\theta \\right) - \\log(2) \\vphantom{\\dfrac 1 2}, \n",
    "\\end{align*}\n",
    "\n",
    "où JS est la *divergence de Jensen-Shannon* (version symétrisée de KL)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On se retrouve donc, si on a un discriminateur parfait, à calculer la divergence de JS entre les lois $p^*$ et $q_\\theta$ : on cherche donc à la minimiser, pour que $q_\\theta$ soit la plus proche possible de $p^*$.\n",
    "On va donc chercher $\\min_\\theta V^*(p^*, q_\\theta)$.\n",
    "\n",
    "Or $V^*$ est elle-même solution d'un problème de maximisation : $V^* = \\max_\\varphi V_\\varphi$.\n",
    "On va alors chercher \n",
    "\n",
    "$$\n",
    "\\min_\\theta \\max_\\varphi V_\\varphi = \\min_\\theta \\max_\\varphi \\left\\{ \\frac 1 2 \\mathbb{E}_{x \\sim p^*} \\left[ \\log\\big(D_\\varphi(x)\\big) \\right] + \\frac 1 2 \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\log\\big(1 - D_\\varphi(x)\\big) \\right] \\right\\},\n",
    "$$\n",
    "\n",
    "ou encore en passant par les variables latentes $z$ :\n",
    "\n",
    "$$\n",
    "\\min_\\theta \\max_\\varphi V_\\varphi = \\min_\\theta \\max_\\varphi \\left\\{ \\frac 1 2 \\mathbb{E}_{x \\sim p^*} \\left[ \\log\\big(D_\\varphi(x)\\big) \\right] + \\frac 1 2 \\mathbb{E}_{z \\sim q} \\left[ \\log\\big(1 - D_\\varphi(G_\\theta(z))\\big) \\right] \\right\\}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 3. Entraînement des GAN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "On a maintenant déterminé le problème d'optimisation que l'on doit résoudre pour déterminer les paramètres $\\varphi$ du réseau discriminateur $D_\\varphi$ ainsi que les paramètres $\\theta$ du réseau générateur $G_\\theta$ : **comment entraîner ces réseaux** ?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On réécrit le problème d'optimisation sous la forme \n",
    "\n",
    "$$\n",
    "\\min_\\theta \\max_\\varphi V(\\theta, \\varphi).\n",
    "$$\n",
    "\n",
    "C'est un jeu à somme nulle :\n",
    "- le générateur, via $\\theta$, va minimiser $V$ ;\n",
    "- le discriminateur, via $\\varphi$, va maximiser $V$.\n",
    "\n",
    "Les gains de l'un sont les pertes de l'autre."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 3.1. Jeu à somme non nulle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Cependant, en pratique, il est difficile de trouver un équilibre de Nash sur un jeu à somme nulle en utilisant des descentes de gradient : on va donc modifier le problème d'optimisation pour qu'il ne soit plus un jeu à somme nulle.\n",
    "\n",
    "On introduit artificiellement deux fonctions de coût $\\mathcal{L}_D$ (pour le discriminateur) et $\\mathcal{L}_G$ (pour le générateur),\n",
    "et on essaye de ramener le calcul de \n",
    "\n",
    "$$\n",
    "\\min_\\theta \\max_\\varphi V(\\theta, \\varphi)\n",
    "$$\n",
    "\n",
    "aux calculs de \n",
    "\n",
    "$$\n",
    "\\min_\\varphi \\mathcal{L}_D(\\theta, \\varphi) \\quad \\text{ et } \\quad \\min_\\theta \\mathcal{L}_G(\\theta, \\varphi).\n",
    "$$\n",
    "\n",
    "Clairement, si $V = \\mathcal{L}_G = -\\mathcal{L}_D$, on retrouve la formulation à somme nulle."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Pour sortir de la somme nulle, on repart de la formule du jeu à somme nulle :\n",
    "\n",
    "$$\n",
    "\\min_\\theta \\max_\\varphi \\left\\{ \\frac 1 2 \\mathbb{E}_{x \\sim p^*} \\left[ \\log\\big(D_\\varphi(x)\\big) \\right] + \\frac 1 2 \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\log\\big(1 - D_\\varphi(x)\\big) \\right] \\right\\}.\n",
    "$$\n",
    "\n",
    "Le générateur, avec ses paramètres $\\theta$, n'influe que sur le second membre de l'addition.\n",
    "\n",
    "Son but est de minimiser la probabilité que le discriminateur classifie ses données générées comme fausses : $G_\\theta$ cherche donc à réaliser $\\min_\\theta \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\log\\big(1 - D_\\varphi(x)\\big) \\right]$.\n",
    "\n",
    "On peut aussi donner comme but au générateur de maximiser la probabilité que le discriminateur classifie ses données générées comme vraies : $G_\\theta$ chercherait alors à réaliser $\\min_\\theta \\mathbb{E}_{x \\sim q_\\theta} \\left[ - \\log\\big(D_\\varphi(x)\\big) \\right]$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "En plus de sortir de la somme nulle, cette formulation permet d'avoir des gradients (en orange sur le graphe) plus forts lorsque $D_\\varphi(G_\\theta(z))$ est faible.\n",
    "\n",
    "<center><div><img src=\"img/jeu_somme_non_nulle.png\" width=\"1250\"/></div></center>\n",
    "\n",
    "En effet, si $D_\\varphi(G_\\theta(z))$ est faible, alors la donnée générée par $G_\\theta$ a été classifiée comme fausse, et les paramètres $\\theta$ doivent être modifiés.\n",
    "Cependant, si le gradient est faible (comme sur les courbes bleues), la descente de gradient aura du mal à modifier les paramètres..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On obtient donc la formulation suivante :\n",
    "\n",
    "$$\n",
    "\\begin{cases}\n",
    "    \\mathcal{L}_D(\\theta, \\varphi) = - \\mathbb{E}_{x \\sim p^*} \\left[ \\log\\big(D_\\varphi(x)\\big) \\right] - \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\log\\big(1 - D_\\varphi(x)\\big) \\right], \\\\\n",
    "    \\mathcal{L}_G(\\theta, \\varphi) = - \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\log\\big(D_\\varphi(x)\\big) \\right],\n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "que l'on peut écrire sous la forme générique suivante :\n",
    "\n",
    "$$\n",
    "\\begin{cases}\n",
    "    \\mathcal{L}_D(\\theta, \\varphi) = \\mathbb{E}_{x \\sim p^*} \\left[ f\\big(D_\\varphi(x)\\big) \\right] + \\mathbb{E}_{x \\sim q_\\theta} \\left[ g\\big(D_\\varphi(x)\\big) \\right], \\\\\n",
    "    \\mathcal{L}_G(\\theta, \\varphi) = \\mathbb{E}_{x \\sim q_\\theta} \\left[ h\\big(D_\\varphi(x)\\big) \\right],\n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "et le problème d'optimisation à résoudre est alors\n",
    "$$\n",
    "\\min_\\varphi \\mathcal{L}_D(\\theta, \\varphi) \\quad \\text{ et } \\quad \\min_\\theta \\mathcal{L}_G(\\theta, \\varphi).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### 3.2. Descente de gradient"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Maintenant que le problème d'optimisation a été réécrit, on en cherche la solution, i.e. on cherche à entraîner les deux réseaux $D_\\varphi$ et $G_\\theta$ pour en trouver les paramètres optimaux.\n",
    "\n",
    "On rappelle que $D_\\varphi$ a été introduit pour minimiser une distance entre $p^*$ et $q_\\theta$.\n",
    "Pour que $q_\\theta$ soit une bonne approximation de $p^*$, il faudrait que $D_\\varphi$ soit entraîné au maximum à chaque mise à jour des paramètres $\\theta$.\n",
    "Cependant, cette stratégie serait très coûteuse, et on adopte plutôt un entraînement partiel de $D_\\varphi$ pendant les étapes d'entraînement."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On aboutit alors à l'algorithme suivant :\n",
    "\n",
    "<center><div><img src=\"img/algo_GAN_sans_precisions.png\" width=\"1500\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Plus précisément, il faut savoir comment calculer les gradients de $\\mathcal{L}_D$ et $\\mathcal{L}_G$.\n",
    "\n",
    "Pour $\\mathcal{L}_D$, on a :\n",
    "\n",
    "\\begin{align*}\n",
    "\\nabla_\\varphi \\mathcal{L}_D(\\theta, \\varphi) &= \\nabla_\\varphi \\mathbb{E}_{x \\sim p^*} \\left[ f\\big(D_\\varphi(x)\\big) \\right] + \\nabla_\\varphi \\mathbb{E}_{x \\sim q_\\theta} \\left[ g\\big(D_\\varphi(x)\\big) \\right], \\vphantom{\\frac 1 2}\\\\\n",
    "                                             &= \\mathbb{E}_{x \\sim p^*} \\left[ \\nabla_\\varphi f\\big(D_\\varphi(x)\\big) \\right] + \\mathbb{E}_{x \\sim q_\\theta} \\left[ \\nabla_\\varphi g\\big(D_\\varphi(x)\\big) \\right]. \\vphantom{\\frac 1 2}\n",
    "\\end{align*}\n",
    "\n",
    "Les gradients de $D_\\varphi$ sont alors calculés par rétro-propagation et les espérances via Monte-Carlo. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Et pour $\\mathcal{L}_G$, on a :\n",
    "\n",
    "\\begin{align*}\n",
    "\\nabla_\\theta \\mathcal{L}_G(\\theta, \\varphi) &= \\nabla_\\theta \\mathbb{E}_{x \\sim q_\\theta} \\left[ h\\big(D_\\varphi(x)\\big) \\right], \\vphantom{\\frac 1 2} \\\\\n",
    "                                             &= \\nabla_\\theta \\mathbb{E}_{z \\sim q} \\left[ h\\big(D_\\varphi(G_\\theta(z))\\big) \\right], \\vphantom{\\frac 1 2} \\\\\n",
    "                                             &= \\mathbb{E}_{z \\sim q} \\left[ \\nabla_\\theta h\\big(D_\\varphi(G_\\theta(z))\\big) \\right]. \\vphantom{\\frac 1 2}\n",
    "\\end{align*}\n",
    "\n",
    "Ceci nous fait aboutir à l'algorithme suivant."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"img/algo_GAN.png\" width=\"1500\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 4. Défauts des GAN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Les fonctionnent bien pour générer des images, mais ont tout de même des défauts : notamment, le *mode collapse* et le *mode hopping*.\n",
    "Les GAN de Wasserstein améliorent grandement ces défauts !\n",
    "\n",
    "<center><div><img src=\"img/mode_collapse.png\" width=\"1250\"/></div></center>"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
