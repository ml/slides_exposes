\documentclass[11pt]{beamer}
\usetheme{Boadilla}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}
%\usepackage{enumitem}
\setbeamertemplate{itemize item}[circle]

\newtheorem{prop}{Proposition}
\newtheorem{hyp}{Hypothèse}
\newtheorem{lemme}{Lemme}
\newtheorem{remarque}{Remarque}
\newtheorem{defi}{Définition}
\usepackage{cancel}
\usepackage{ulem}
\usepackage{dsfont}

\makeatother
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.25\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.65\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertshorttitle
  \end{beamercolorbox}}%
    \begin{beamercolorbox}[wd=.1\paperwidth,ht=2.25ex,dp=1ex,center]{date in head/foot}%
     \insertframenumber{} / \inserttotalframenumber
  \end{beamercolorbox}%
  \vskip0pt%
}
\makeatletter

\global\long\def\v#1{\boldsymbol{#1}}%
\author[]{}
\title{Réseaux de neurones multi-perceptron}
%\setbeamercovered{transparent} 
\setbeamertemplate{navigation symbols}{} 
%\subject{} 
\date{27 janvier 2021}


\global\long\def\Ns{\mathbb{N^*}}
\global\long\def\b{\color{blue}\textbf}
\global\long\def\demo{\underline{Démonstration :}~}
\global\long\def\ex{\underline{Exemple :}~}
\definecolor{mygray}{gray}{0.8}
\global\long\def\x{\color{mygray}}
\global\long\def\g{\color{green}}
\global\long\def\o{\overline}
\begin{document}


\AtBeginSection[]{
  \begin{frame}{Plan}
  \small \tableofcontents[currentsection]
  \end{frame} 
}






\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\tableofcontents
\end{frame}






\begin{frame}{Régression ou classification ?}
\begin{itemize}
\item {\b{Régression}} :
permet de prédire une \textbf{valeur numérique}
\ex Estimer le prix d'une maison

~~

~~

\item {\b{Classification}} :
permet de prédire l'appartenance à une \textbf{classe} ou une \textbf{catégorie}


\ex Classer des images selon des catégories
\end{itemize}
\end{frame}





\section{Régression linéaire}

\begin{frame}{Régression linéaire}
On souhaite prédire le \textbf{label} ou la \textbf{cible} $y$ selon les \textbf{caractéristiques} (ou \textit{features}) $\v x = (x_1, \dots, x_d)$.



Pour cela, on suppose que :
\begin{itemize}
\item la relation entre les variables $\v x$ et $y$ est {\b{linéaire}}.
\begin{block}{}
$$y = b + \sum_{k=1}^d w_k x_k + bruit$$
\end{block}
On appelle $w_k$ les \textbf{poids}, et $b$ le \textbf{biais}.
\item le bruit suit une {\b{distribution gaussienne}}.
\end{itemize}
\end{frame}




\begin{frame}{Exemple}

On souhaite estimer le prix d'une maison, en fonction de sa surface.

~~

On va trouver une base de données avec les prix et les surfaces de plusieurs maisons. 
Ce sera notre {\b{jeu de données d'entrainement}} (ou \textit{training set}).

~~

Notre objectif est de trouver les paramètres $\v w$ et $b$ de notre modèle.

~~

Ensuite, on pourra donner une estimation du prix $\hat{y}$ de n'importe quelle maison dont on connaît la surface :
\begin{block}{}
$$\hat{y}=b + \sum_{k=1}^d w_k x_k$$
\end{block}
\end{frame}


\begin{frame}{Exemple}
Supposons que l'on dispose des données suivantes :

\begin{center}
\includegraphics[width=0.5\textwidth]{data.png}
\end{center}

Chaque point correspond à un élément $(\v x^{(i)},y^{(i)})$ de la base de données.

~~

On souhaite trouver les paramètres $\v w$ et $b$ de la droite qui va être la plus proche possible des données. 

Mais comment dire si une droite est "proche" des données ?
\end{frame}




\begin{frame}{Les moindres carrés}
On utilise généralement la fonction des {\b{moindres carrés}} :
\begin{block}{}
$$L(\v w,b)
=\frac{1}{n} \sum_{i=1}^n \ell^{(i)} (\v w, b)
=\frac{1}{n} \sum_{i=1}^n \frac{1}{2} \left( \hat{y}^{(i)}-{y}^{(i)} \right)^2$$
\end{block}
\begin{center}
\includegraphics[width=0.3\textwidth]{moindres_carre.png}
\end{center}
Comme $\hat y^{(i)}=b + \sum_{k=1}^d w_k x_k^{(i)}$, on a :
$$\ell^{(i)} (\v w, b)=\frac{1}{2} \left( b + \v w^T \v x^{(i)}-{y}^{(i)} \right)^2$$
\end{frame}

\begin{frame}{Problème de minimisation}
Finalement, on souhaite trouver les paramètres $(\v w^*, b^*)$ qui minimise la fonction \textit{loss} :
\begin{block}{}
$$(\v w^*, b^*)= \text{argmin}_{\v w,b} L(\v w,b)$$
\end{block}


avec 
$$
L(\v w,b)
=\frac{1}{n} \sum_{i=1}^n 
\frac{1}{2} \left( b + \v w^T \v x^{(i)}-{y}^{(i)} \right)^2
$$

\end{frame}






\begin{frame}{Solution analytique}

Si on ajoute une colonne de $1$ à la matrice $\v x$ et que l'on inclut le biais $b$ dans le vecteur des paramètres $\v w$, on doit simplement résoudre le problème de minimisation :
$$\min_{\v W} ~ ||\v y - \v X \v W ||^2$$

Si $n>d$, alors la solution est :
\begin{block}{}
$$\v W^*= \left(\v  X^T \v X \right)^{-1}\v  X^T\v  y$$
\end{block}
\end{frame}


\begin{frame}{Algorithme du gradient}
Mais on peut également résoudre ce problème de minimisation, avec un {\b{algorithme du gradient}}.

A chaque itération, on actualise les valeurs des paramètres $(\v w, b)$ par :
\begin{block}{}
$$
(\v w, b) \leftarrow (\v w, b)
- \frac{\eta}{n} \sum_{i=1}^n \partial_{(\v w, b)} \ell^{(i)} (\v w, b)
$$
\end{block}

où $\eta$ est le {\b{taux d'apprentissage}} (ou \textit{learning rate}).
\end{frame}


\begin{frame}{Algorithme du gradient minibatch}

Cepandant, cette méthode peut être très longue, lorsque l'on dispose de beaucoup de données (et donc $n$ est grand). On peut alors effectuer une
{\b{descente de gradient minibatch}}.

~~

A chaque itération, on choisit aléatoirement un sous-échantillon $\mathcal{B}$ de données, pour lequel on calcule le gradient :
\begin{block}{}
$$
(\v w, b) \leftarrow (\v w, b)
- \frac{\eta}{|\mathcal{B}|} \sum_{i \in \mathcal{B}} \partial_{(\v w, b)} \ell^{(i)} (\v w, b)
$$
\end{block}

\end{frame}


\begin{frame}{Exemple}

En appliquant l'algorithme du gradient précédent sur les données, on obtient la droite ci-dessous en $3$ \textit{epochs}, c'est-à-dire en parcourant $3$ fois toutes les données:
\begin{center}
\includegraphics[width=0.7\textwidth]{data_regression.png}
\end{center}

\end{frame}


\begin{frame}{Réseau de neurones d'une régression linéaire}
On peut schématiser une régression linéaire par le réseau de neurones à 1 couche suivant :

~~

\begin{center}
\includegraphics[width=0.8\textwidth]{regression.png}
\end{center}

On cherche les $w_{i}$ et le $b$ tels que :
\begin{align*}
o_1=x_1 w_{1}+x_2 w_{2} + x_3 w_{3} + b \\
\end{align*}
\end{frame}



\section{Classification linéaire}


\begin{frame}{Exemple}
On dispose d'une base de données contenant des images de vêtements,  ainsi qu'un \textit{label}, c'est-à-dire la catégorie associée à chaque image.
\begin{center}
\includegraphics[width=0.5\textwidth]{data_classification.png}
\end{center}

On pourrait noter les classes :
$\{\text{t-shirt, pantalon, pull}\}$

Mais on choisit de les noter :
$\{\text{(1,0,0), (0,1,0), (0,0,1)}\}$

\end{frame}

\begin{frame}{Réseau de neurones d'une classification linéaire}
\begin{center}
\includegraphics[width=0.5\textwidth]{softmax.png}
\end{center}

On suppose que chaque sortie $o_i$ dépend linéairement des entrées $x_j$, c'est-à-dire que l'on cherche les $w_{ij}$ et les $b_i$ tels que :
\begin{align*}
o_1=x_1 w_{11}+x_2 w_{12} + x_3 w_{13} + b_1 \\
o_2=x_1 w_{21}+x_2 w_{22} + x_3 w_{23} + b_2 \\
o_3=x_1 w_{31}+x_2 w_{32} + x_3 w_{33} + b_3
\end{align*}


\end{frame}

\begin{frame}{Argmax}

On peut ensuite voir le vecteur de solutions 
$(o_1,o_2,o_3)$ comme les probabilités d'appartenir à la classe $1$, $2$ ou $3$

~~

Une première solution est donc de choisir comme prédiction la classe qui a la plus grande probabilité :
\begin{block}{}
$$\hat{y}_j = \text{argmax}_j~o_j$$
\end{block}

\pause
Cependant :
\begin{itemize}
\item $o_j$ peut être négatif.
\item La somme des $o_j$ n'est pas forcément égale à $1$.
\end{itemize}
\end{frame}


\begin{frame}{Softmax}
La fonction {\b{softmax}} permet de transformer le vecteur 
$\v{o}$ en un vecteur de probabilités :
\begin{block}{}
$$\v{\hat{y}} = {softmax}(o) \text{~~~~~~ avec ~~~~~~}
\hat{y}_j = \frac{\exp(o_j)}{\sum_{k}\exp(o_k)}$$
\end{block}
\end{frame}


\begin{frame}{Vraisemblance}

$\hat{y_j}$ est donc la probabilité qu'un élément avec les caractéristiques $\v x$ appartienne à la classe $j$, représentée par le vecteur $e_j$ :
$$\hat{y_j}=P(\v y=e_j|\v x)$$

Notre objectif est donc de maximiser la {\b{vraisemblance}} :
\begin{block}{}
$$
P(Y|X) = \prod_{i=1}^n P(\v{y^{(i)}}|\v{x^{(i)}})
$$
%&=\prod_{i=1}^n \prod_{j=1}^q P\left(\v y^{(i)}=\v e_j|\v x^{(i)}\right)^{\delta_{\v y^{(i)}=\v e_j}}

\end{block}


\end{frame}

\begin{frame}{Log-vraisemblance}

C'est équivalent à minimiser la fonction de {\b{log-vraisemblance}} :

%$$-\log(P(Y|X)) = - \sum_{i=1}^n \log(P(\v{y^{(i)}}|\v{x^{(i)}}))
%=\sum_{i=1}^n \ell(\v{y^{(i)}},\v{\hat{y}^{(i)}})
%$$
%
%avec :
%\begin{block}{}
%$$\ell(\v{y^{(i)}},\v{\hat{y}^{(i)}})
%=-\sum_{j=1}^q y_j \log(\hat{y}_j)
%$$
%où $q$ est le nombre de classes.
%\end{block}

\begin{align*}
-\log(P(Y|X)) &=- \sum_{i=1}^n \log\left(P\left(\v y^{(i)}|\v x^{(i)}\right)\right) \\
&=-\sum_{i=1}^n \log\left(\prod_{j=1}^q P\left(\v y^{(i)}=\v e_j|\v x^{(i)}\right)^{\delta_{\v y^{(i)}=\v e_j}}\right) \\
&=-\sum_{i=1}^n \sum_{j=1}^q y_j^{(i)} \log\left(P\left(\v y^{(i)}=\v e_j|\v x^{(i)}\right)\right) 
\end{align*}

\begin{block}{}
$$-\log(P(Y|X)) =-\sum_{i=1}^n \sum_{j=1}^q y_j^{(i)} \log\left(\hat{y}_j^{(i)}\right)$$
\end{block}


\end{frame}




\begin{frame}{Précision}
La {\b{précision}} (ou \textit{accuracy}) correspond à la proportion d'éléments qui ont été prédits correctement :

\begin{block}{}
$$\frac{1}{n} \sum_{i=1}^n \mathds{1}_{\{y_i=\hat{y}_i\}} $$
\end{block}

\pause
On peut calculer la précision que l'on obtient sur le jeu de données d'entrainement (ou \textit{training set}).

~~

Mais il est intéressant de calculer la précision avec un nouveau jeu de données test (ou \textit{testing set}), qui n'a pas servi à entraîner notre modèle.

~~

\begin{block}{}
Avant d'entraîner un modèle, on sépare le jeu de données en 2 : 
\begin{itemize}
\item un \textit{training set} ($70$ à $80\%$)
\item un \textit{testing set} ($20$ à $30\%$)
\end{itemize}
\end{block}

\end{frame}




\begin{frame}{Exemple}

On dispose de $784$ images de vêtements, réparties en $10$ classes.

~~


\begin{figure}
\includegraphics[width=0.5\textwidth]{classification_loss_acc.png}
\end{figure}

L'accuracy test est de $0.843$.

Sur les données tests, on obtient les prédictions suivantes :
\begin{figure}
\includegraphics[width=0.6\textwidth]{classification_prediction.png}
\end{figure}
\end{frame}





\section{Multi-perceptron}


\begin{frame}
Dans les modèles précédents, on suppose que la cible $y$ dépend linéairement des caractéristiques $\v x$.

 Mais si cette dépendance n'est pas linéaire ?
\end{frame}



\begin{frame}{Réseaux de neurones multi-perceptron}

On peut ajouter une {\b{couche de neurones cachée}} (ou \textit{hidden layer}).
\begin{figure}
\includegraphics[width=0.8\textwidth]{mlp.png}
\end{figure}

On appelle ce modèle {\b{multilayer perceptron}} ou MLP.

On dit que ce modèle possède $2$ couches de neurones.

Ce modèle est \textit{fully connected}.
\end{frame}




\begin{frame}{Fonction d'activation}

Si on utilise le modèle précédent, on obtient les mêmes résultats que sans la couche cachée. En effet, une transformation affine d'une transformation affine est une transformation affine.

~~\pause

Pour supprimer la linéarité, on ajoute une {\b{fonction d'activation}}  $\sigma$ sur les neurones des couches cachées.





\begin{center}
   \begin{tabular}{| c | c | c |}
     \hline
     Rectified Linear Unit & 
     Sigmoid & 
     Hyperbolic tangent\\ \hline
     $ReLU(x)=\max(x,0)$ & 
     $\text{sigmoid}(x)=\frac{1}{1+\exp(-x)}$ & 
     $\tanh(x)=\frac{1-\exp(-2x)}{1+\exp(-2x)}$ \\ \hline
     \includegraphics[width=0.3\textwidth]{relu.png}  &
     \includegraphics[width=0.3\textwidth]{sigmoid.png}  &
     \includegraphics[width=0.3\textwidth]{tanh.png} \\
     \hline
   \end{tabular}
 \end{center} 
\end{frame}






\begin{frame}{Exemple}

On reprend la même base d'images que pour l'exemple de classification linéaire. 

Avec une couche cachée de $256$ neurones, la fonction d'activation $ReLU$, $10$ \textit{epochs} et un \textit{learning rate} $lr=0.1$, 
on obtient :

~~


\begin{figure}
\includegraphics[width=0.3\textwidth]{mlp_loss_acc.png}
\end{figure}

Sur les données tests, on obtient les prédictions suivantes :
\begin{figure}
\includegraphics[width=0.6\textwidth]{mlp_prediction.png}
\end{figure}
\end{frame}

\begin{frame}{Exemple : Nombre de neurones cachées}

Le nombre de neurones cachées est un {\b{hyperparamètre}}, que l'on peut faire varier.

\begin{center}
   \begin{tabular}{| c | c |}
     \hline
     Nombre neurones cachées & Accuracy test
     \\ \hline
     256 & 0.8736
     \\ \hline
     128 & 0.8569
     \\ \hline
     8 & 0.8398
     \\ \hline
   \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Exemple : Nombre de couches de neurones cachées}

On peut également ajouter d'autres couches de neurones cachées.

~~

Par exemple, si on considère des couches cachées de $8$ neurones, et que l'on effectue $30$ epochs, on obtient :
\begin{center}
   \begin{tabular}{| c | c |}
     \hline
     Nombre de couches de neurones cachées & Accuracy test
     \\ \hline
     1 & 0.8367
     \\ \hline
     2 & 0.8448
     \\ \hline
     %3 & 
     %\\ \hline
   \end{tabular}
\end{center}
\end{frame}


\begin{frame}{Exemple : Changement de la fonction d'activation}

Changeons maintenant la fonction d'activation.

On considère $20$ epochs, et une couche cachée de $256$ neurones.

\begin{center}
   \begin{tabular}{| c | c |}
     \hline
     Fonction d'activation & Accuracy test
     \\ \hline
     ReLU & 0.8685
     \\ \hline
     Sigmoid & 0.8465
     \\ \hline
     Tanh & 0.8701
     \\ \hline
   \end{tabular}
\end{center}
\end{frame}



\begin{frame}{Exemple : Conclusion}

Il faut faire varier les hyperparamètres :
\begin{itemize}
\item Nombre de neurones cachées,
\item Nombre de couches de neurones,
\item Nombre d'epochs,
\item Fonction d'activation,
\item Taux d'apprentissage,
\end{itemize}
et trouver ceux qui nous donnent les meilleurs résultats !

\end{frame}




\begin{frame}{Conclusion}
\begin{itemize}
\item Les modèles MLP sont différentiables par rapport aux entrées $\v x$.
\item Le gradient par rapport aux poids $\v w$ se calcule assez facilement.
\item Avec une couche caché contenant suffisament de neurones, on peut approcher n'importe quelle fonction.
\end{itemize}

\end{frame}


\begin{frame}{Références}
\nocite{*} 
\footnotesize{
 \bibliographystyle{abbrv}
\bibliography{ref}
}
\end{frame}



\end{document}