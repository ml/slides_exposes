{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Conservative PINNs, $hp$-VPINNs and wPINNs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 1/ Conservative PINNs\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [Jagtap, Kharazmi, Karniadakis, 2020]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: Combine PINNs with domain decomposition, while ensuring global conservation.\n",
    "\n",
    "To that end, conservative PINNs ensure flux continuity at the interfaces between subdomains. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"PINNs_vs_cPINNs.png\" width=\"1200\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**For a standard PINN**, the output of the neural network $\\mathcal{N}$, defined on the whole domain, is given by\n",
    "\n",
    "$$\n",
    "    u_{\\theta}(z) = \\mathcal{N} (z; \\theta), \n",
    "$$ \n",
    "\n",
    "with $z$ containing all the physical parameters (position, viscosity, ...). \n",
    "\n",
    "Conversely, **for a cPINN**, we consider a domain decomposed in $N_{sd}$ subdomains. The output of the neural network $\\mathcal{N}^s$, defined on the $s$-th subdomain, is denoted by\n",
    "\n",
    "$$\n",
    "    u_{\\theta^s}(z) = \\mathcal{N}^s (z; \\theta^s),\n",
    "$$\n",
    "\n",
    "and the final output is \n",
    "\n",
    "$$\n",
    "    u_{\\theta}(z) = \\bigcup_{s = 1}^{N_{sd}} u_{\\theta^s}(z). \n",
    "$$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**For a standard PINN**, the loss function looks something like\n",
    "\n",
    "$$\n",
    "    L_{PINN} = \\omega_\\partial \\text{ MSE}_\\partial + \\omega_r \\text{ MSE}_r,\n",
    "$$\n",
    "\n",
    "with the $\\omega_*$ denoting (adaptive) weights, and the mean squared errors being standard: $\\text{MSE}_\\partial$ takes care of the boundary/initial points, and $\\text{MSE}_r$ controls the residual of the PDE under consideration."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**For a cPINN**, we end up with one loss function per subdomain $s$:\n",
    "\n",
    "$$\n",
    "    L_{cPINN}^s =\n",
    "    \\omega_\\partial^s \\text{ MSE}_\\partial^s +\n",
    "    \\omega_r^s \\text{ MSE}_r^s +\n",
    "    \\omega_i^s \\big( \\text{MSE}_{\\text{flux}}^s + \\text{ MSE}_{\\text{avg}}^s\\big).\n",
    "$$\n",
    "\n",
    "Besides the standard boundary/initial and residual losses, we have two additional losses controlling what happens at the interface between domains.\n",
    "We define:\n",
    "- $N_i$ the number of interface points in subdomain $s$,\n",
    "- $x_i^s$ the interface points,\n",
    "- $s_i^+$ the neighboring subdomain at the $i$-th interface point,\n",
    "- $f$ the flux function of the PDE: we have $u_t + f(u, u_x, u_{xx}, \\dots ; \\lambda_1, \\lambda_2, \\dots)_x = 0$.\n",
    "\n",
    "The two losses are then given by:\n",
    "\n",
    "\\begin{align*}\n",
    "    \\text{MSE}_{\\text{flux}}^s & =\n",
    "    \\frac 1 {N_i} \\sum_{i = 1}^{N_i} \\left| f(u_{\\theta^s}(x_i^s)) \\cdot n - f(u_{\\theta^{s_i^+}}(x_i^s)) \\cdot n \\right|^2, \\\\\n",
    "    \\text{MSE}_{\\text{avg}}^s & =\n",
    "    \\frac 1 {N_i} \\sum_{i = 1}^{N_i} \\left| u_{\\theta^s}(x_i^s) - \\frac{u_{\\theta^s}(x_i^s) + u_{\\theta^{s_i^+}}(x_i^s)} 2 \\right|^2.\n",
    "\\end{align*}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The first loss, $\\text{MSE}_{\\text{flux}}^s$, ensures the **continuity of the flux at the interfaces**: hence, **the conservation property is satisfied** (up to some error).\n",
    "\n",
    "The second loss, $\\text{MSE}_{\\text{avg}}^s$, is similar to the flux continuity in the case of smooth solutions. \n",
    "However, for discontinuous solutions, it helps adjacent networks capture the average value of the discontinuous solution at the interface."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "_Summary_: Advantages of cPINNs over PINNs\n",
    "\n",
    "- reduce the approximation error by selecting the network size and hyperparameters for each subdomain\n",
    "- reduce the generalization error by carefully selecting the number and location of interface points\n",
    "- reduce the optimization error by solving a simpler optimization problem in each subdomain\n",
    "- parallelize the computation with subdomains\n",
    "- able to handle inverse problems (not presented here)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"cPINN_Burgers.png\" width=\"1200\"/></div></center>\n",
    "\n",
    "<center><div><img src=\"cPINN_Burgers_losses.png\" width=\"1200\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2a/ VPINNs\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [Kharazmi, Zhang, Karniadakis, 2019]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: Write the equation under a variational form to help with discontinuities, and to reduce the order of the derivatives in the loss function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The following PDE is considered:\n",
    "\n",
    "\\begin{align*}\n",
    "    \\mathcal{L} \\ u(x, t) = f(x, t) &\\quad \\text{ for } (x, t) \\in \\Omega \\times (0, T], \\\\\n",
    "    u(x, t) = h(x, t) &\\quad \\text{ for } (x, t) \\in \\partial\\Omega \\times (0, T], \\\\\n",
    "    u(x, 0) = g(x) &\\quad \\text{ for } x \\in \\Omega.\n",
    "\\end{align*}\n",
    "\n",
    "Approximate solutions to this PDE, given by a neural network, will be denoted by $\\tilde u$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Approximate solutions should make the following strong residuals vanish (or should make them approach zero):\n",
    "\n",
    "\\begin{align*}\n",
    "    r(\\tilde u) := \\mathcal{L} \\ \\tilde u - f &\\quad \\forall (x, t) \\in \\Omega \\times (0, T], \\\\\n",
    "    r_b(\\tilde u) := \\tilde u - h &\\quad \\forall (x, t) \\in \\partial\\Omega \\times (0, T], \\\\\n",
    "    r_0(\\tilde u) := \\tilde u - g &\\quad \\forall x \\in \\Omega.\n",
    "\\end{align*}\n",
    "\n",
    "Minimizing those residuals is the approach chosen by **standard PINNs**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In contrast, **variational PINNs** (VPINNs) instead minimize the weak formulation of the main residual: taking some test functions $(v_k)_{1 \\leq k \\leq K}$, it is defined for all $1 \\leq k \\leq K$ by\n",
    "\n",
    "$$\n",
    "    \\mathcal{R}_k(\\tilde u) := \\int_{\\Omega \\times (0, T]} r(\\tilde u) \\, v_k \\, dx \\, dt.\n",
    "$$\n",
    "\n",
    "This leads to a nonlinear system, which can be reformulated as a minimization problem by introducing a cost function corresponding to a weighted sum of the above weak residuals."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The **oss function** is then given by \n",
    "\n",
    "$$\n",
    "    L = \n",
    "    \\omega_r \\frac 1 K \\sum_{k = 1}^{K} \\left| \\mathcal{R}_k(\\tilde u) \\right|^2 +\n",
    "    \\omega_b \\frac 1 {N_b} \\sum_{i = 1}^{N_{b}} \\left| r_b(x_b^i, t_b^i) \\right|^2 +\n",
    "    \\omega_0 \\frac 1 {N_0} \\sum_{i = 1}^{N_{0}} \\left| r_0(x_0^i) \\right|^2,\n",
    "$$\n",
    "\n",
    "with $x_b$ (resp. $x_0$) the collocation points for the boundary (resp. initial) conditions, of which there are $N_b$ (resp. $N_0$): the last two parts of the loss are standard.\n",
    "\n",
    "The first part is non-standard: the strong residual has been replaced with the weak loss, averaged over the $K$ test functions.\n",
    "\n",
    "**Question**: how to compute $\\mathcal{R}_k(\\tilde u)$ in practice?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let us take the **example of Poisson's equation**: in this case, $\\mathcal{L} \\ u = - \\Delta u$.\n",
    "In 1D, we obtain \n",
    "\n",
    "$$\n",
    "    r(\\tilde u) = - \\partial_{xx} \\tilde u - f,\n",
    "$$\n",
    "\n",
    "and so the weak residual is \n",
    "\n",
    "$$\n",
    "    \\mathcal{R}_k(\\tilde u)\n",
    "    =\n",
    "    \\int_{\\Omega} (- \\partial_{xx} \\tilde u - f) \\ v_k\n",
    "    =\n",
    "    - \\underbrace{\\int_{\\Omega} \\partial_{xx} \\tilde u \\ v_k}_{\\mathcal{R}_{\\mathcal{L},k}(\\tilde u)}\n",
    "    - \\underbrace{\\int_{\\Omega} f \\ v_k}_{\\mathcal{R}_{f,k}(\\tilde u)}.\n",
    "$$\n",
    "\n",
    "To compute $\\mathcal{R}_{f,k}(\\tilde u)$, one just needs adequate sampling.\n",
    "\n",
    "For $\\mathcal{R}_{\\mathcal{L},k}(\\tilde u)$, one can **use integration by parts to reduce the order of the derivatives**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Indeed, by **successive integration by parts**, we obtain \n",
    "\n",
    "\\begin{align*}\n",
    "    \\mathcal{R}_{\\mathcal{L},k}^{(1)}(\\tilde u) & = \\int_{\\Omega} \\partial_{xx} \\tilde u \\ v_k, \\\\\n",
    "    \\mathcal{R}_{\\mathcal{L},k}^{(2)}(\\tilde u) & = - \\int_{\\Omega} \\partial_{x} \\tilde u \\ \\partial_x v_k + \\left( \\partial_{x} \\tilde u \\ v_k \\right)_{| \\partial \\Omega}, \\\\\n",
    "    \\mathcal{R}_{\\mathcal{L},k}^{(3)}(\\tilde u) & = \\int_{\\Omega} \\tilde u \\ \\partial_{xx} v_k - \\left( \\tilde u \\ \\partial_x v_k \\right)_{| \\partial \\Omega} + \\left( \\partial_{x} \\tilde u \\ v_k \\right)_{| \\partial \\Omega},\n",
    "\\end{align*}\n",
    "\n",
    "where the additional index increases as successive integration by parts are performed.\n",
    "\n",
    "The boundary terms are no issue, since we assume that the $v_k$ are compactly supported, and since $\\tilde u$ can be set to be equal to $u$ at the boundary.\n",
    "\n",
    "The advantage of this formulation is to **reduce the order of derivatives** on $\\tilde u$, as evaluating higher-order derivtives increases computation time.\n",
    "\n",
    "The loss function becomes\n",
    "\n",
    "$$\n",
    "    L^{(p)} = \n",
    "    \\omega_r \\frac 1 K \\sum_{k = 1}^{K} \\left| \\mathcal{R}^{(p)}_{\\mathcal{L},k}(\\tilde u) \\right|^2 +\n",
    "    \\omega_r \\frac 1 K \\sum_{k = 1}^{K} \\left| \\mathcal{R}_{f,k}(\\tilde u) \\right|^2 +\n",
    "    \\omega_b \\frac 1 {N_b} \\sum_{i = 1}^{N_{b}} \\left| r_b(x_b^i, t_b^i) \\right|^2 +\n",
    "    \\omega_0 \\frac 1 {N_0} \\sum_{i = 1}^{N_{0}} \\left| r_0(x_0^i) \\right|^2.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"VPINN_Poisson.png\" width=\"900\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"VPINN_Poisson_sharp.png\" width=\"900\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2b/ $hp$-VPINNs\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [Kharazmi, Zhang, Karniadakis, 2021]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: Combine VPINNs with domain decomposition and increase accuracy on each subdomain."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Equipped with the above (general) definition of a VPINN, a $hp$-VPINN is based on a specific choice of test functions.\n",
    "We first partition the set $V = \\Omega \\times (0, T]$ (or $\\Omega$) in $N_{sd}$ subdomains $(V_s)_{1 \\leq s \\leq N_{sd}}$, and define the test functions $(v_{k,s})_{1 \\leq s \\leq N_{sd}, 1 \\leq k \\leq K_s}$ as follows:\n",
    "\n",
    "$$\n",
    "    v_{k,s} = \n",
    "    \\begin{cases}\n",
    "        \\bar v_k \\neq 0 & \\text{ in } V_s, \\\\\n",
    "        0 & \\text{ in } V_s^c,\n",
    "    \\end{cases}\n",
    "$$\n",
    "\n",
    "with $V_s^c$ the complement of $V_s$, such that $V_s \\cup V_s^c = V$.\n",
    "\n",
    "This leads to a subdomain method, with vanishing residuals, by construction, on the other subdomains.\n",
    "The non-vanishing test functions $\\bar v_k$ are polynomials, whose order can be chosen by the user.\n",
    "Contrary to standard VPINNs, the number $K_s$ of test functions can depend on the domain $s$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The loss function is then given by \n",
    "\n",
    "$$\n",
    "    L = \n",
    "    \\omega_r \\sum_{s = 1}^{N_{sd}} \\frac 1 {K_s} \\sum_{k = 1}^{K_s} \\left| \\mathcal{R}_{k,s}(\\tilde u) \\right|^2 +\n",
    "    \\omega_b \\frac 1 {N_b} \\sum_{i = 1}^{N_{b}} \\left| r_b(x_b^i, t_b^i) \\right|^2 +\n",
    "    \\omega_0 \\frac 1 {N_0} \\sum_{i = 1}^{N_{0}} \\left| r_0(x_0^i) \\right|^2.\n",
    "$$\n",
    "\n",
    "The last two parts of the loss are standard; the non-standard part of the loss is the first one:\n",
    "- it consists in a sum over the subdomains $s$,\n",
    "- $K_s$ is the number of test functions in the subdomain $s$,\n",
    "- and $\\mathcal{R}_{k,s}$ is the weak residual computed with the $k$-th test function on the subdomain $s$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"hp-VPINN_function_approximation.png\" width=\"1200\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"hp-VPINN_discontinuous_function_approximation.png\" width=\"1200\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"hp-VPINN_Poisson2D.png\" width=\"800\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 3/ wPINNs: weak PINNs\n",
    "\n",
    "<br>\n",
    "\n",
    "<u>Main reference</u>: [De Ryck, Mishra, Molinaro, 2022]\n",
    "\n",
    "<br>\n",
    "\n",
    "_Goal_: Improve the handling of discontinuous solutions and help entropy satisfaction.\n",
    "\n",
    "To that end, wPINNs approximate the entropy solutions to conservation laws. For the moment, they have been introduced on scalar conservation laws."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We consider the following scalar conservation law:\n",
    "\n",
    "$$\n",
    "    \\tag{$\\ast$}\n",
    "    u_t + f(u)_x = 0, \n",
    "$$\n",
    "\n",
    "supplemented with initial data $u_0$ and suitable (e.g. periodic) boundary conditions.\n",
    "\n",
    "<ins>Definition [weak solution]:</ins> $u \\in L^\\infty(\\mathbb{R} \\times \\mathbb{R}_+)$ is a **weak solution** to $(\\ast)$ with initial data $u_0 \\in L^\\infty(\\mathbb{R})$ if, for all test functions $\\varphi \\in \\mathcal{C}_c^1(\\mathbb{R} \\times \\mathbb{R}_+)$,\n",
    "\n",
    "$$\n",
    "    \\int_{\\mathbb{R}_+} \\int_{\\mathbb{R}} (u \\varphi_t + f(u) \\varphi_x) \\, dx \\, dt\n",
    "    + \\int_{\\mathbb{R}} u_0(x) \\varphi(x, 0) \\, dx = 0.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We consider the following scalar conservation law:\n",
    "\n",
    "$$\n",
    "    \\tag{$\\ast$}\n",
    "    u_t + f(u)_x = 0, \n",
    "$$\n",
    "\n",
    "supplemented with initial data $u_0$ and suitable (e.g. periodic) boundary conditions.\n",
    "\n",
    "<ins>Definition [entropy solution]:</ins> $u \\in L^\\infty(\\mathbb{R} \\times \\mathbb{R}_+)$ is an **entropy solution** to $(\\ast)$ with initial data $u_0 \\in L^\\infty(\\mathbb{R})$ if it is a weak solution to $(\\ast)$, and if it satisfies, for all test functions $\\varphi \\in \\mathcal{C}_c^1(\\mathbb{R} \\times \\mathbb{R}_+)$, for all $c \\in \\mathbb{R}$ and for all $T > 0$:\n",
    "\n",
    "$$\n",
    "    \\int_0^T \\int_{\\mathbb{R}} \\Big(|u - c| \\, \\varphi_t + Q[u; c] \\varphi_x\\Big) \\, dx \\, dt\n",
    "    - \\int_{\\mathbb{R}} \\Big( |u(x, T) - c| \\, \\varphi(x, T) - |u_0(x) - c| \\, \\varphi(x, 0) \\Big) \\, dx \\geq 0,\n",
    "$$\n",
    "\n",
    "where $Q: (u, c) \\in \\mathbb{R}^2 \\mapsto \\text{sgn}(u - c) (f(u) - f(c)) \\in \\mathbb{R}$ is the Kruzhkov entropy flux.\n",
    "Under some non-restrictive hypotheses, we can prove that _the entropy solution is unique_."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "A **wPINN** consists in replacing the pointwise residual, unsuited to the approximation of discontinuous solutions, by a residual based on the weak formulation instead:\n",
    "\n",
    "$$\n",
    "    \\mathcal{R}(v, \\varphi, c)\n",
    "    :=   \n",
    "    - \\int_0^T \\int_\\Omega \\Big( \\big| v - c \\big| \\varphi_t + Q[v; c] \\varphi_x \\Big).\n",
    "$$\n",
    "\n",
    "If $u$ is an entropy solution, $\\mathcal{R}(u, \\varphi, c) \\leq 0$ holds for all $\\varphi$ and $c$.\n",
    "Therefore, with $u_\\theta$ the prediction made by the neural network, the new optimization problem to get the optimal weights $\\theta^\\ast$ reads:\n",
    "\n",
    "$$\n",
    "    \\theta^\\ast :=\n",
    "    \\text{arg}\\min_{\\theta}\n",
    "    \\Big[ \\max_{\\varphi, c} \\Big(\\mathcal{R}(u_\\theta, \\varphi, c) \\Big)\n",
    "    + \\text{ boundary and initial losses} \\Big].\n",
    "$$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "An advantage of this formulation is that **rigorous bounds can be proven on different sources of error** associated to wPINNs: see [De Ryck, Mishra, Molinaro, 2022] for the complete description.\n",
    "\n",
    "Basically, if the network is large and trained on enough random collocation points, the errors can be made arbitrarily small."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In practice, the $\\max$ in the min-max problem resulting from the wPINN residual has to be evaluated over a function space: $\\varphi \\in W_0^{1, \\infty} (\\Omega \\times [0, T])$.\n",
    "This function space is infinite-dimensional!\n",
    "\n",
    "An idea to represent functions from $W_0^{1, \\infty} (\\Omega \\times [0, T])$ is to **train another neural network** to approximate the realization of the $\\max$.\n",
    "\n",
    "In practice, we restrict the choice of the test function $\\varphi$ to the parameterized family $\\varphi_\\eta = \\gamma \\xi_\\eta$, where $\\xi_\\eta$ is a neural network with trainable parameters $\\eta$, and where $\\gamma: \\Omega \\to \\mathbb{R}$ is a smooth cut-off function with the following properties:\n",
    "\n",
    "$$\n",
    "    \\gamma(x) =\n",
    "    \\begin{cases}\n",
    "        1 & \\text{ if } x \\in \\Omega_\\varepsilon = \\{ x \\in \\Omega: \\text{dist}(x, \\partial \\Omega) < \\varepsilon\\}, \\\\\n",
    "        0 & \\text{ if } x \\in \\partial\\Omega,\n",
    "    \\end{cases}\n",
    "$$\n",
    "\n",
    "which ensure that the test function $\\varphi_\\eta$ has compact support.\n",
    "\n",
    "For the parameter $c \\in \\mathbb{R}$ in the optimization problem, the $\\max$ is realized over a finite set of values $c_\\text{min} \\leq c_i \\leq c_\\text{max}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"wPINN_Burgers.png\" width=\"1200\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<center><div><img src=\"wPINN_Burgers_best.png\" width=\"1200\"/></div></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<ins>Conclusion:</ins> pros and cons of wPINNs:\n",
    "\n",
    "- more expensive to train than standard PINNs (because of the $\\min$-$\\max$ rather than a simple $\\min$)\n",
    "- able to provide a better approximation of discontinuous solutions\n",
    "- easily extendable to multidimensional scalar conservation laws since Kruzhkov entropies are well-defined\n",
    "- the extension to systems is non-trivial: there is no analogue of Kruzhkov entropies for systems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 4/ Comparison of the four approaches (PINNs, cPINNs, $hp$-VPINNs, wPINNs)\n",
    "\n",
    "All five approaches are able to handle smooth solutions, on a single domain, of scalar conservation laws (or more general equations).\n",
    "\n",
    "|  | PINNs | cPINNs | VPINNs | $hp$-VPINNs | wPINNs |\n",
    "| :- | :-: | :-: | :-: | :-: | :-: |\n",
    "| subdomains | ✘ | ✓ | ✘ | ✓ | ✘ |\n",
    "| discontinuous | ✘ | ~ | ✓ | ✓ | ✓ |\n",
    "| entropy | ✘ | ✘ | ✘ | ✘ | ✓ |\n",
    "| system | ✓ | ✓ | ✓ | ✓ | ✘ |\n",
    "| no $\\min$-$\\max$ | ✓ | ✓ | ✓ | ✓ | ✘ |"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "vscode": {
   "interpreter": {
    "hash": "0adcc2737ebf6a4a119f135174df96668767fca1ef1112612db5ecadf2b6d608"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
