## Exécuter les notebooks

### En ligne

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fml%2Fslides_exposes.git/master)

### En local

Cloner le projet :

```bash
git clone https://gitlab.math.unistra.fr/ml/slides_exposes.git
cd slides_exposes/
```

`python 3.8` est actuellement nécessaire pour tensorflow.
On peut l'installer facilement avec [Conda](https://www.anaconda.com/products/individual) ou [Miniconda](https://docs.conda.io/en/latest/miniconda.html) (plus léger).

Créer et activer un environnement virtuel avec  :

```bash
pip install virtualenv
virtualenv -p py3.8 .venv
source .venv/bin/activate
```

Installer les dépendances :

```bash
pip install -r requirements.txt
```

Lancer un serveur Jupyter :

```bash
jupyter-notebook
```

### Construire le jupyter-book localement

#### Version html

```bash
jupyter-book build jupyterbook/
```

#### Version pdf

```bash
jupyter-book build jupyterbook/ --builder pdflatex
```

Documentation sur <https://jupyterbook.org>.
